<?php

// values that need some calculation and therefore 
// do not appear verbatim in general-values.scad
// must be read calculated from openscad 

$output = shell_exec("openscad config/bom-values.scad -o bom-values.stl 2>&1");

preg_match_all('/ECHO: "(.*)", ([0-9.]+)/' ,$output, $matches);

for ($i=0; $i < count($matches[1]); $i++) {
    $calculated_values[$matches[1][$i]] = $matches[2][$i];
}

if (isset($argv[1])) $bom_path = $argv[1]; else $bom_path = "config/bill-of-materials.org";

// initialize error status variable
$error = 0;

$bom = file_get_contents($bom_path);

// get general values file and read it. Does it reference another general 
// values file?
$general_values = file_get_contents('general-values.scad');
preg_match('/include <(general-values-n2\.scad)>/', $general_values, $matches);

// if it does redirect to that new general files
if (count($matches)>0) $general_values = file_get_contents($matches[1]);

// find all variables (starting with $) in the bill of materials
preg_match_all('/\$[\w]*/', $bom, $matches);


//iterate over them and find their values in general-values.scad
foreach($matches[0] as $match){
    // get rid of the dollar sign
    $searchFor = substr($match, 1);
    
    //TODO: Here we check a list of variables that should be calculated
    if (isset($calculated_values[$searchFor])) {
        $bom = str_replace($match, $calculated_values[$searchFor], $bom);
    } else {
        $pattern = '/'.$searchFor.'[\s]?=[\s]?"?([\w]+)"?;/';
        preg_match($pattern, $general_values, $int_matches);
    
        if (count($int_matches)==0) {
            fwrite(STDERR, "Value $match not found \n");
            $error = 1;
        } else $bom = str_replace($match, $int_matches[1], $bom);
    }
    
    
}

echo "\n";
echo $bom;

exit($error);