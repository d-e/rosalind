include <../general-values.scad>
use <../parts/x-axis-clamp.scad>
include <calculated-values.scad>
use <../lib/utl.NEMA.scad>

echo("internal" , external - profile_width*2);
echo("internalZ" , externalZ - profile_width*2);
echo("y_rod_length" , external-profile_width);
echo("x_rod_length" , x_rod_length());
echo("no_z_rods" , bed_platform_supports*2);
echo("z_rod_length" , externalZ-profile_width/2);
echo("x_bed_frame_length" , _bed_platform_length-2*bed_profile-3*s);
echo("y_bed_frame_length" , bed_platform_width);
echo("bed_insulation_length" , bed_insulation_length);
echo("bed_insulation_width" , bed_insulation_width);
echo("leadscrew_d" , leadscrew[0]);
echo("leadscrew_l" , leadscrew[2]);
echo("bed_assembly_thickness" , bed_assembly_thickness);
echo("no_z_bearings" , bed_platform_supports*4);