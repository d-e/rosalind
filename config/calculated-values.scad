/**
* This file contains value calculations that are being used 
* throughout the printer
*
*/

include <../general-values.scad>
use <../lib/mechanical.scad>
use <../parts/x-axis-clamp.scad>
use <../parts/y-axis-clamp.scad>
use <../parts/x-carriage.scad>
use <../assemblies/extruder.scad>
use <../parts/extruder-drive-direct.scad>

//FIXME: refactor small_angle_fastener_height out.
y_axis_clamp_z_offset = max(
        frame_angle_fastener_side+profile_width+y_rod_d,
        profile_width+frame_angle_fastener_side
    );

x_axis_clamp_small_offset = x_axis_clamp_size()[0]-x_axis_clamp_y_offset();
extruder_filler = (extruder_nema_length-(profile_width-acrylic_position));
air = z_rod_d;
y_min = flip_secondary_axis?
        0 //TODO: fix this
    :
        max(
            y_axis_clamp_offset()[1] + x_axis_clamp_small_offset,
            max(
                nema_shaft+extruder_filler, 
                nema17_size()[1]/2+z_rod_d/2+air
            )+y_rod_d/2 + air +x_carriage_size()[1]/2+extruder_mount_size()[1]+object_cooling_fan_size()[1]
        );
        
y_max = external - 2*profile_width - y_axis_clamp_y_min() - max(y_axis_clamp_y_min(), endstop_height) - x_axis_clamp_y_offset();

echo("y_min", y_min);
echo("y_max", y_max);

bed_assembly_thickness = insulation_thickness + glass_thickness + heated_bed_pcb_thickness + bed_levelling_tolerance;

z_home = externalZ-y_axis_clamp_z_offset-x_carriage_size()[2]/2-bed_assembly_thickness-extruder_offset;
y_home = flip_secondary_axis?y_max:y_min;
x_home = x_axis_clamp_size()[1]/2+x_carriage_size()[0]/2; //this starts from the center of the Y rods

echo("y_home", y_home);
echo("x_home", x_home);
echo("z_home", z_home);

_bed_platform_length=bed_platform_length=="max"?external-2*(45+5):bed_platform_length=="auto"?heated_bed_length-2*profile_width:bed_platform_length;
bed_insulation_length = _bed_platform_length+2*bed_profile;

bed_dist_y = (bed_platform_supports==2) ?
    min(autoy,bed_platform_width):
    external-bed_profile-nema17_size()[0]-bed_profile-leadscrew[0]*2;

bed_insulation_width = bed_platform_supports==2?bed_dist_y:bed_dist_y-(bed_dist_y-heated_bed_width)/2;



z_axis_screw_clamp_size = externalZ-nema_length-nema_shaft-leadscrew[2]+bearing_size(leadscrew_bearing)[2];

/* unused */
//bottom_Y_rail_Z = external-y_axis_clamp_z_offset-x_carriage_rod_distance/2-bed_profile/2;
// y_axis_clamp_z_offset = profile_width+x_axis_clamp_height()/2+y_axis_clamp_size()[2]-y_axis_clamp_offset()[2];
// y_carriage_block_height = bearing_size(y_linear_bearing)[1] + 5 * ceil(minimum_thickness/2);
//y_max = external - profile_width - y_min;