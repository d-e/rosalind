/**
 * This file just contains some more calculated values that 
 * I couldn't add to `calculated_values.scad` because they 
 * caused recursion
 */

y_clamps_angle_height = max(profile_width/2,idler_diameter+3);
y_clamps_slab_height = profile_width-acrylic_position+y_clamps_angle_height;
x_carriage_rod_distance = 2*y_rod_d+4*minimum_thickness+x_rod_d;
echo(str("x_carriage_rod_distance = ", x_carriage_rod_distance ));

extruder_drive_thickness = minimum_thickness + drive_gear_length - drive_gear_hobbed_offset - 0.5;

