/*

     Rosalind: A parametric CoreXY 3D printer
     Copyright (C) 2017  Michael Demetriou

     This file is part of Rosalind.

     Rosalind is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     Rosalind is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with Rosalind. If not, see <http://www.gnu.org/licenses/>.   

                                                                            */

use <vitamins/aluminium-profile.scad>
use <parts/angle-fastener.scad>
use <parts/x-axis-clamp.scad>
use <parts/z-axis-clamp.scad>
include <lib/PRZutility.scad>
use <lib/utl.NEMA.scad>
use <assemblies/bed.scad>
use <parts/motor-mounts.scad>
use <parts/x-carriage.scad>
use <parts/y-axis-clamp.scad>
use <lib/mechanical.scad>
include <config/calculated-values.scad>
include <general-values.scad>
use <assemblies/extruder-drive-geared.scad>
use <parts/extruder-drive-direct.scad>
use <parts/bowden-extruder.scad>
include <vitamins/belt.scad>
use <parts/LCD-cover-ORIGINAL-MK3.scad>
use <parts/lcd-supports.scad>
use <lib/lcd_case.scad>
use <parts/faceplate.scad>
include <lib/colors.scad>
use <lib/functions.scad>
use <lib/angle-fastener.scad>
use <assemblies/base.scad>

// calculate the width of the printer without the aluminium frame 
internal = external-profile_width*2; 

// the printer is square when viewed top-down but it can have a different height
internalZ = externalZ-profile_width*2;

// Set up the position of the nozzle
// The units are relative to the home position (i.e. 0,0 is a valid value)
positionY = 0; //max: 229
positionX = 50; //max: 262
positionZ = 0; //max: 160 (n2)

// if the Y axis is flipped then the actual Y might be the revers of positionY
Y = flip_secondary_axis?-positionY:positionY;

//echo (str("Z Home: ",z_home));
half_nema = nema17_size(nema_length)[0]/2;

// before starting rendering let's echo some data
echo("X axis rod length: ", x_rod_length());

// Z motor
translate([external/2,half_nema+profile_width,0]) Zmotor();
if (bed_platform_supports==2) translate([external/2,-half_nema+profile_width+internal,0]) mirror([0,1,0]) Zmotor();

frame();
translate([0,0,-base_plate_thickness]) base();

//Y axis clamps
y_axis_clamps();

//z axes
zclamps();
//x-carriage
x_carriage_positioned();
//acrylic_and_angles();
motors();
bed_positioned();

//show or hide the extruder (speeds up rendering)
show_extruder=true;

extruder_motor_x = profile_width+extruder_drive_size()[1]/2+frame_angle_fastener_side+s;
extruder_motor_y = profile_width;
extruder_motor_z = externalZ-profile_width-nema17_size()[0]/2;

translate([extruder_motor_x, extruder_motor_y, extruder_motor_z]) rotate(90) extruder_drive(true, true);

translate([external-extruder_motor_x, extruder_motor_y, extruder_motor_z]) mirror([1,0,0]) rotate(90)  extruder_drive(true, true);

lcd_display_assembly();

faceplate_positioned();

/**
* Render the extruder drive 
*
*/
module extruder_drive() {
    if (extruder_drive == "geared")
        extruder_drive_geared();
    else if (extruder_drive == "direct")
        extruder_drive_direct();
}

/**
* Render one z-axis motor along with a (smooth) representation
* of the Z leadscrew
*
*/
module Zmotor() {
        translate([0,0,nema_length+nema_shaft/2]) coupler();
        rotate(90) translate([0,0,externalZ-profile_width/2-z_axis_clamp_size()[2]/2]) z_axis_screw_clamp(z_axis_screw_clamp_size, save_material=true);
        rail() translate([0,0,nema_length+nema_shaft]) cylinder(d=leadscrew[0], h=leadscrew[2]);
}

/**
* Render the aluminium frame 
*
*/
module frame(){
    echo("Vertical T-Slot profiles (4 pieces): (mm)", externalZ);
    echo("Horizontal T-Slot profiles (8 pieces): (mm)", external-2*profile_width);
    translate([0,0,externalZ/2]){
        //Z
        translate([-s,-s,0]) aluminium_profile(externalZ);
        translate([0,external-profile_width+s,0]) aluminium_profile(externalZ);
        translate([external-profile_width+s,external-profile_width+s,0]) aluminium_profile(externalZ);
        translate([external-profile_width+s,0,0]) aluminium_profile(externalZ);
    }
    translate([(external)/2,0,externalZ]) rotate([0,90,0]){
        //X
        XYframe();
    }
    XYangles();
    verticalAngles();
    translate([(external),(external)/2,externalZ]) rotate([0,90,90]){
        //Y
        XYframe();
    }
    translate([0,0,externalZ-profile_width]){
        XYangles();
    }
}

/**
* Render the clamps that hold the x-axis in place and move
* along the y axis (y-carriage)
*
* @param flip bool Whether the y-axis is flipped to the opposite direction
*                  or not. 
*
*/
module y_carriage(flip=false){
    x1 = flip?internal-y_axis_clamp_x_offset():y_axis_clamp_x_offset();
    x2 = flip?y_axis_clamp_x_offset():internal-y_axis_clamp_x_offset();
    pos = flip?positionX+x_home:x2-positionX-x_home;
    alurodY = (x_axis_clamp_size()[0]-x_axis_clamp_y_offset());
    translate([0,Y+y_home,0]){
        translate([x2, 0, 0]) {
            rotate(flip?180:0) x_axis_clamp(side="left", position=pos);
        }
        translate([x1, 0, 0]) {
            rotate(flip?180:0) x_axis_clamp(side="right", position=pos);
        }
    }
    
}

/**
* Render the frame-mounted clamps that hold the Y-axis rails in place
*
*/
module y_axis_clamps(){
    2_y_axis_clamps();
    translate([external,0,0]) mirror([1,0,0]) 2_y_axis_clamps(endstop=true);

    //Y rods
    translate([profile_width,profile_width,externalZ-y_axis_clamp_z_offset]){
        rail() translate([y_axis_clamp_x_offset(), -profile_width/2, 0]) rotate([-90,0,0]) cylinder(d=y_rod_d, h=external-profile_width);
        rail() translate([internal-y_axis_clamp_x_offset(), -profile_width/2, 0]) rotate([-90,0,0]) cylinder(d=y_rod_d, h=external-profile_width);
        //x-axis clamp  (y-carriage)
        y_carriage(flip=flip_secondary_axis);
    }
}

/**
* Render the X and Y motors
*
*/
module motors(){ 
    translate([0,0,0]) XYmotor();
    translate([external,0,-belt_distance-belt_width]) mirror() XYmotor("down");
}

/**
* Render the angles that fasten the acrylic enclosure in place 
*
*/
module acrylic_and_angles(){
    translate([profile_width,acrylic_position,internal]) rotate([0,0,0]) mirror() angle_fastener([3,profile_width-acrylic_position,20],profile_width/4,8,4, stiffeners = 2);
}

/**
* Render the bed assembly in it's right place
*
*/
module bed_positioned(){
    translate([external/2,external/2,z_home-positionZ]) bed();
}

/**
* Render the x-carriage (the part that carries the nozzle around) at it's final position
*
*/
module x_carriage_positioned(){
    translate([profile_width+y_axis_clamp_x_offset()+positionX+x_home, Y+y_home+profile_width, externalZ -y_axis_clamp_z_offset]) {
        x_carriage(rails=false, bearings=true);
        if(show_extruder) if (flip_secondary_axis) translate([0,x_carriage_size()[1]/2,0]) bowden_extruder();
        else translate([0,-x_carriage_size()[1]/2,0]) mirror([0,1,0]) bowden_extruder();
    }
}

/**
* Render one of the motors that move the X and Y axes. In CoreXY configuration both motors
* work to move both axes.
*
* @param side string One of the XY motors is a bit lower as it moves the lower belt. This
*                    parameter controls which one the render motor is.
*
*/
module XYmotor(side="up"){
    nema_offset = [ -half_nema+profile_width+XY_motor_mount_slab_thickness()+s,
                    external-half_nema-profile_width-s,
                    externalZ-y_axis_clamp_z_offset-teeth_offset-x_axis_clamp_size()[2]/2-idler_height+idler_flange_thickness ];
    
    translate(nema_offset) rotate([0,0,0]) {
        translate([0,0,-s]) nema17(height=nema_length-2*s);
        translate([0,0,teeth_offset-2]) difference(){
            vitamin() cylinder(d=idler_diameter, h=10);
            cylinder(d=motor_shaft_diameter+s, h=10);
        }
        rotate(90) XY_motor_mount();
    }
    belts(side);
    
    module belts(){
        y_clamp_idler = [ profile_width+y_axis_clamp_x_offset()+y_axis_clamp_idler_offset()[0],
                      y_axis_clamp_y_min()+profile_width-idler_diameter/2+belt_thickness ];
    
        x_clamp_idler = [ profile_width+y_axis_clamp_x_offset()-x_axis_clamp_width()/2-idler_diameter/3+belt_thickness, 
                          positionY+y_home+profile_width+x_axis_clamp_idler_center()[0] ];

        // color("red") translate([0, positionY+y_home+profile_width, externalZ-50]) cube(10);
        
        translate([0,0,nema_offset[2]+teeth_offset]) {
            belt([[ nema_offset[0]-idler_diameter/2, nema_offset[1] ],  y_clamp_idler-[idler_diameter/2,0]], show_belts = show_belts);
            
            rad = (idler_diameter/2 + belt_thickness/2);
            
            belt([y_clamp_idler-[0, rad],
                 [external-y_clamp_idler[0], y_clamp_idler[1]-rad]], show_belts = show_belts);
            
            belt([nema_offset+[idler_diameter/2,0],x_clamp_idler], show_belts = show_belts);
        }
        sig = side=="down"?-1:1;
        translate([0,0,nema_offset[2]+teeth_offset+(-belt_distance-belt_width)*sig]) {
            belt([x_clamp_idler,
                 [profile_width+y_axis_clamp_x_offset()+y_axis_clamp_idler_offset()[0]-idler_diameter/2,
                  -y_axis_clamp_idler_offset()[0]]], show_belts = show_belts);
        }
    }
}

/**
* Render the clamps that hold the z-axis smooth rails in place
*
*/
module zclamps(rods=true){
    zclampz = profile_width/2-z_axis_clamp_size()[2]/2;
    translate([0,0,zclampz]) zclampslevel(rods);
    translate([0,0,zclampz+internalZ+profile_width+z_axis_clamp_size()[2]]) mirror([0,0,1]) zclampslevel(rods=rods, endstop=true);
}

/**
* Render the clamps that hold the z-axis smooth rails in place for the top or bottom of the printer
* this module is then translated and called again for the other "level"
*
* @param rods    bool Whether to render the steel rails
* @param endstop bool Whether this level has the endstop holding clamp or not (usually on the upper level)
*
*/
module zclampslevel(rods=false, endstop=false){
    translate([external/2,0,0]){
        zclampsaxis(rods, endstop);
        if (bed_platform_supports==2) translate([0,external,0]) mirror([0,1,0]){
           zclampsaxis(rods); 
        }
    }
}

/**
* Render the clamps that hold the z-axis smooth rails in place for a single axis
* this module is then translated and called again for the other axis and then for
* top/bottom. It is quite simple and just mirrors around the center of the printer
* See the comment of zclamp() module for more.
*
* @param rods    bool Whether to render the steel rails
* @param endstop bool Whether this axis has the endstop holding clamp or not (usually on the upper level)
*
*/
module zclampsaxis(rods=false, endstop=false){
    zclamp(rods, endstop);
    mirror() zclamp(rods);
}

/**
* Render a single z-axis smooth rail clamp. The origin of htis module is the center
* of the back side of the printer. This makes it easier to align the clamps to the
* bed (which is always in the middle)
*
* @param rod     bool   Whether to render the steel rail 
* @param endstop bool  Whether this particular clamp has an endstop holder extension
*
* @return  
*/
module zclamp(rod=false, endstop=false){
    zclampx = profile_width+z_axis_clamp_x_mountpoint_offset();
    // this translates from the middle of the printer and the other modules
    // just mirror stuff
    translate([-bed_x_clamps_position()[0],zclampx,0]) rotate([0,0,90]) {
        z_axis_clamp(endstop, print = false);
        if (rod) color("silver") cylinder(d=z_rod_d, h=externalZ-profile_width/2);
    }
}

/**
* Render the aluminium frame on the X and Y axes.
*
*/
module XYframe(){
    translate([0,-s,s]) aluminium_profile(external-2*profile_width);
    translate([0,external-profile_width,0]) aluminium_profile(external-2*profile_width-s);
    translate([externalZ-profile_width,external-profile_width,0]) aluminium_profile(external-2*profile_width);
    translate([externalZ-profile_width,0,0]) aluminium_profile(external-2*profile_width);
}

/**
* Render the angles that connect the X and Y aluminium profile parts
*
*/
module XYangles(){
    rotate([0,0,-90]) translate([-profile_width-s,profile_width+s,0]) frame_angle();
    rotate([0,0,90]) translate([external-profile_width-s,profile_width+s-external,0]) frame_angle();
    rotate([0,0,180]) translate([-profile_width-s,profile_width-external+s,0]) frame_angle();
    rotate([0,0,0]) translate([external-profile_width-s,profile_width+s,0]) frame_angle();
}

/**
* Render the angles that connect the vertical with the horizontal frame parts
*
*/
module verticalAngles(){
    /**
    * One side of the plane frame (1 dimension)
    */   
    module side(){
        translate([0, profile_width+s, profile_width+s]) rotate([0,90,0]) frame_angle();
        translate([profile_width, external-profile_width-s, profile_width+s]) rotate([0,90,180]) frame_angle();
    }
    /**
    * One plane (2D)
    */
    module plane(){
        side();
        translate([profile_width+s,0,externalZ]) rotate([0,180,0]) side();
    }
    plane();
    translate([external-profile_width-s,0,0]) plane();
    rotate(90) translate([external-profile_width-s,-external,0]) plane();
    rotate(90) translate([0,-external,0]) plane();
}

/**
* Render one of the clamps that hold the y-axis smooth rods
*
* @param rotY      float     The rotation around Y of the specific clamp. Used to flip to render the opposite side (the other rail).
* @param rotZ      float     The rotation around Z of the specific clamp. Used to flip to render the other end of this side. 
* @param idler     bool      Whether this specific clamp has an idler shelf.
* @param endstop   bool      Whether this specific clamp holds an endstop switch.
*
*/
module YAxisClampP(rotY=0,rotZ,idler = false, endstop = false){
    translate([profile_width+y_axis_clamp_x_offset()+s,profile_width/2+s,externalZ-y_axis_clamp_z_offset]) rotate([0,rotY,0]) rotate(rotZ) y_axis_clamp(idler, endstop=endstop);
}

/**
* Render a side of y-rail clamps
*
* @param rotY     float  The rotation around Y of this side of rails
* @param endstop  bool   Whether one of the clamps of this side holds and endstop switch
*
*/
module 2_y_axis_clamps(rotY, endstop=false){
    YAxisClampP(rotY, rotZ=0, idler = true, endstop = endstop?flip_secondary_axis:false);
    translate([0,external,0]) mirrory() YAxisClampP(rotY, rotZ=0, endstop = endstop?!flip_secondary_axis:false);  
}

/**
* Render the lcd display along with it's enclosure and frame mounts
*
*/
module lcd_display_assembly(){
    translate([external-profile_width, external, 0]) lcd_supports();
    translate([external-profile_width-105, external+38, 20]) rotate([270+45, 0, 0]) rotate(180) lcd_display();
    translate([external-profile_width-105, external+45, 55]) rotate([135, 0, 0]) rotate(180) lcd_cover();
}

/**
* Render the name faceplate of the printer
*
*/
module faceplate_positioned(){
    translate([external, external, externalZ-faceplate_size()[2]]) rotate([-90, 0, 0]) rotate(180) faceplate();
}