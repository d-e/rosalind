endstop_height =     endstop_model=="makerbot" ? 16.2 : 16.2;
endstop_width =      endstop_model=="makerbot" ? 40   : 20  ;
endstop_thickness =  endstop_model=="makerbot" ? 7.5  : 6   ;

module endstop(type, show = true){
    vitamin(show) if (type=="makerbot"){
        translate([0,0,0.7]) %import("../lib/MakerBot_Endstop.stl");
        echo ("makerbot endstop");
        endstop_holes();
    } else if (type=="simple"){
        echo ("simple endstop switch");
        translate([10,12,6]) rotate([90,180,180]) import("../lib/Endstop_Switch.stl");
    } else if (type=="custom"){
        echo ("custom endstop switch", endstop_custom_stl);
        translate(custom_endstop_translation_matrix) rotate(custom_endstop_rotation_matrix) import(endstop_custom_stl);
    } else echo("Error, no endstop configured");
}

module endstop_holes(depth=5, out=true){
    diam=out?3.5:3;
    translate([3,2.5,0]) cylinder(d=diam,h=out?1:depth, $fn=12);
    translate([3,13.5,0]) cylinder(d=diam,h=out?1:depth, $fn=12);
    translate([17,2.5,0]) cylinder(d=diam,h=depth, $fn=12);
    translate([36.5,2.5,0]) cylinder(d=diam,h=depth, $fn=12);
}
