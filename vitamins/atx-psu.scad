e=0.01;

psu_size = [150, 140, 86];

hole_downleft_pos = [6, 0, 16];
hole_downright_pos = [120, 0 , 6];
hole_upleft_pos = [6, 0, 80];
hole_upright_pos = [144, 0, 80];

screw_l = 10;

atx_psu();

module atx_psu(){
    difference(){
        cube(psu_size);
        translate([0,screw_l-e,0]) {
            translate(hole_downleft_pos) rotate([90,0,0]) {
                cylinder(d=3, h=screw_l);
            }
            translate(hole_downright_pos) rotate([90,0,0]) {
                cylinder(d=3, h=screw_l);
            }
            translate(hole_upleft_pos) rotate([90,0,0]) {
                cylinder(d=3, h=screw_l);
            }
            #translate(hole_upright_pos) rotate([90,0,0]) {
                cylinder(d=3, h=screw_l);
            }
        }
    }
}

function atx_psu_holes() = [[hole_upleft_pos, hole_upright_pos],
                            [hole_downleft_pos, hole_downright_pos]];