/**
* Modules that preview aluminium t-slot profile for the frame
* and bed of the printer.
*
*/

include <../general-values.scad>
include <../lib/Fractional_T-Slot_20x_mm_OpenSCAD_Library/T-Slot.scad>

/* Preview */

rotate([90,180,90]) aluminium_profile(type="squar",100); 

/* Debug: renders for debugging various options */

// rotate([90,180,90]) custom_alu_profile(30);
// rotate([90,180,90]) translate([profile_width,profile_width,0]) custom_alu_profile(30);

/**
* Render a length of aluminium profile. The profile width is determined
* by the values in `general-values.scad`. For the time being it can only
* support, square, 20x20 t-slot or custom (dxf) cross-section
*
* @param length float The length of the profile to be printer 
*
*/
module aluminium_profile(length, type=custom_profile){
    color("silver") if (type=="t-slot"){
        if (profile_width-20<1) translate([10,10,0]) 2020Profile(length); 
        else square_profile(length);
    } else if (custom_profile!="") {
        custom_alu_profile(length);
    } else {
        // if we don't have a library just display a square section
        square_profile(length);
    }
}

/**
* Render a length of square profile (cube)
*
* @param length float The length of the part 
*
*/
module square_profile(length){
    color("silver") translate([profile_width/2, profile_width/2,0]) cube([profile_width,profile_width,length], center=true);
}

/**
* Renders an extrusion of the `dxf` declared in general-values as custom profile
*
* @param length float The length of the extrusion 
*
*/
module custom_alu_profile(length){
    color("silver") linear_extrude(height = length, center = true, convexity = 0)
        import(str("../",custom_profile));
}

module bed_profile(length){
    if (bed_profile==20) /*translate([10,10,0])*/ 2020Profile(length); 
    //TODO: make positioning consistent with the other module
    // right now this prints centrally and the frame prints at the
    // center of the length but not of the section
}