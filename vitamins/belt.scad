/**
* Timing belt module
*
*/

/* Preview */

//belt([[300,100],[10,17],[20,20],[23,30]],10,1);
//belt([[3,10],[5,20]],10,1);

/**
* Show a (flat, for quick preview) timing belt
*
* @param points    array An array of points through which the belt passes
* @param height    float The width of the belt
* @param thickness float The thickness of the belt
*
*/
module belt(points, height=6, thickness=1, show_belts = true){
    include <../lib/colors.scad>
    if (show_belts) color(belts) for (i = [0:1:len(points)-2]){
        hull(){
            translate([points[i][0],points[i][1],0]) cylinder(d=thickness,h=height);
            translate([points[i+1][0],points[i+1][1],0]) cylinder(d=thickness,h=height);
        }
    }
}

