# Rosalind: A Parametric CoreXY 3D printer

Still a mess, but in the spirit of release early and often, I present you with Rosalind.

She is a CoreXY 3D printer (almost) fully  modelled in OpenSCAD. There is one prototype created, so it works. However I have not yet tested any other configuration except the one it comes shipped it.

You are welcome to help improve it and make it a truly universal 3D printer.

All files except otherwise noted are GPL v2 or later. I have tried to use only free software, if you find anything proprietary or CC-NC in this repository please report.

The printer is named after [Rosalind Franklin](https://en.wikipedia.org/wiki/Rosalind_Franklin) as per RepRap custom.

The prototype Rosalind looks like this ![rosalind](rosalind.jpg) and here it is, printing [aria the dragon](https://www.thingiverse.com/thing:600550): ![rosalind printing aria](rosalind_printing_aria.jpg) 

## Documentation

You'll have to read the comments for now. I'll maybe work on it. No promises.

## Chimera/cyclops support

Mount and cooling funnels for chimera/cyclops are included as separate files [bowden-cyclops-mount.scad](bowden-cyclops-mount.scad) and [lib/nozzle_cooling_cyclops.stl](lib/nozzle_cooling_cyclops.stl) but there's no option to show them on the printer yet.

## Credits

A special shout out to *schlotzz* for the [extruder drive](https://www.thingiverse.com/thing:275593) and *nophead* for [Mendel 90](https://github.com/nophead/Mendel90) from which I got various models.

Geared extruder drive [Greg's B'Wadestruder](https://www.thingiverse.com/thing:1831961/)
