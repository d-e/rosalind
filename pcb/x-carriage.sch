EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:x-carriage-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x02 LASER/SPINDLE0
U 1 1 5A5BAB77
P 2750 1000
F 0 "LASER/SPINDLE0" H 2750 1100 50  0000 C CNN
F 1 "Conn_01x02" H 2750 800 50  0000 C CNN
F 2 "Connectors_JST:JST_XH_B02B-XH-A_02x2.50mm_Straight" H 2750 1000 50  0001 C CNN
F 3 "" H 2750 1000 50  0001 C CNN
	1    2750 1000
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x02 ENDSTOP0
U 1 1 5A5BAD4E
P 2750 1400
F 0 "ENDSTOP0" H 2750 1500 50  0000 C CNN
F 1 "Conn_01x02" H 2750 1200 50  0000 C CNN
F 2 "Connectors_JST:JST_XH_B02B-XH-A_02x2.50mm_Straight" H 2750 1400 50  0001 C CNN
F 3 "" H 2750 1400 50  0001 C CNN
	1    2750 1400
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x02 AUX0
U 1 1 5A5BADD0
P 2750 1800
F 0 "AUX0" H 2750 1900 50  0000 C CNN
F 1 "Conn_01x02" H 2750 1600 50  0000 C CNN
F 2 "Connectors_JST:JST_XH_B02B-XH-A_02x2.50mm_Straight" H 2750 1800 50  0001 C CNN
F 3 "" H 2750 1800 50  0001 C CNN
	1    2750 1800
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x02 AUX1
U 1 1 5A5BAE27
P 5750 1600
F 0 "AUX1" H 5750 1700 50  0000 C CNN
F 1 "Conn_01x02" H 5750 1400 50  0000 C CNN
F 2 "Connectors_JST:JST_XH_B02B-XH-A_02x2.50mm_Straight" H 5750 1600 50  0001 C CNN
F 3 "" H 5750 1600 50  0001 C CNN
	1    5750 1600
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 SERVO0
U 1 1 5A5BAEB2
P 2800 3250
F 0 "SERVO0" H 2800 3350 50  0000 C CNN
F 1 "Conn_01x02" H 2800 3050 50  0000 C CNN
F 2 "Connectors_JST:JST_XH_B02B-XH-A_02x2.50mm_Straight" H 2800 3250 50  0001 C CNN
F 3 "" H 2800 3250 50  0001 C CNN
	1    2800 3250
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x02 TEMP0
U 1 1 5A5BAF91
P 2750 2300
F 0 "TEMP0" H 2750 2400 50  0000 C CNN
F 1 "Conn_01x02" H 2750 2100 50  0000 C CNN
F 2 "Connectors_JST:JST_XH_B02B-XH-A_02x2.50mm_Straight" H 2750 2300 50  0001 C CNN
F 3 "" H 2750 2300 50  0001 C CNN
	1    2750 2300
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x02 TEMP1
U 1 1 5A5BAFF5
P 5750 2300
F 0 "TEMP1" H 5750 2400 50  0000 C CNN
F 1 "Conn_01x02" H 5750 2100 50  0000 C CNN
F 2 "Connectors_JST:JST_XH_B02B-XH-A_02x2.50mm_Straight" H 5750 2300 50  0001 C CNN
F 3 "" H 5750 2300 50  0001 C CNN
	1    5750 2300
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 FAN0
U 1 1 5A5BB079
P 2750 2750
F 0 "FAN0" H 2750 2850 50  0000 C CNN
F 1 "Conn_01x02" H 2750 2550 50  0000 C CNN
F 2 "Connectors_JST:JST_XH_B02B-XH-A_02x2.50mm_Straight" H 2750 2750 50  0001 C CNN
F 3 "" H 2750 2750 50  0001 C CNN
	1    2750 2750
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x02 FAN1
U 1 1 5A5BB0E5
P 5750 2700
F 0 "FAN1" H 5750 2800 50  0000 C CNN
F 1 "Conn_01x02" H 5750 2500 50  0000 C CNN
F 2 "Connectors_JST:JST_XH_B02B-XH-A_02x2.50mm_Straight" H 5750 2700 50  0001 C CNN
F 3 "" H 5750 2700 50  0001 C CNN
	1    5750 2700
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 24V_0
U 1 1 5A5BB166
P 6200 3150
F 0 "24V_0" H 6200 3250 50  0000 C CNN
F 1 "Conn_01x02" H 6200 2950 50  0000 C CNN
F 2 "Connectors_JST:JST_XH_B02B-XH-A_02x2.50mm_Straight" H 6200 3150 50  0001 C CNN
F 3 "" H 6200 3150 50  0001 C CNN
	1    6200 3150
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 24V_1
U 1 1 5A5BB1DB
P 6200 3550
F 0 "24V_1" H 6200 3650 50  0000 C CNN
F 1 "Conn_01x02" H 6200 3350 50  0000 C CNN
F 2 "Connectors_JST:JST_XH_B02B-XH-A_02x2.50mm_Straight" H 6200 3550 50  0001 C CNN
F 3 "" H 6200 3550 50  0001 C CNN
	1    6200 3550
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 HEATER0
U 1 1 5A5BB268
P 5400 4100
F 0 "HEATER0" H 5400 4200 50  0000 C CNN
F 1 "Conn_01x02" H 5400 3900 50  0000 C CNN
F 2 "TerminalBlocks_Phoenix:TerminalBlock_Phoenix_MKDS1.5-2pol" H 5400 4100 50  0001 C CNN
F 3 "" H 5400 4100 50  0001 C CNN
	1    5400 4100
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 HEATER1
U 1 1 5A5BB2F8
P 5400 4600
F 0 "HEATER1" H 5400 4700 50  0000 C CNN
F 1 "Conn_01x02" H 5400 4400 50  0000 C CNN
F 2 "TerminalBlocks_Phoenix:TerminalBlock_Phoenix_MKDS1.5-2pol" H 5400 4600 50  0001 C CNN
F 3 "" H 5400 4600 50  0001 C CNN
	1    5400 4600
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x04 E0
U 1 1 5A5BBB7C
P 5750 1100
F 0 "E0" H 5750 1300 50  0000 C CNN
F 1 "Conn_01x04" H 5750 800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 5750 1100 50  0001 C CNN
F 3 "" H 5750 1100 50  0001 C CNN
	1    5750 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3150 6000 3550
Wire Wire Line
	6000 3250 6000 3650
Wire Wire Line
	4450 2800 6000 3250
Wire Wire Line
	4450 2700 6000 3150
Wire Wire Line
	3950 1700 2950 1000
Wire Wire Line
	3950 1800 2950 1100
Wire Wire Line
	2950 1400 3950 1900
Wire Wire Line
	2950 1500 3950 2000
Wire Wire Line
	3950 2100 2950 1800
Wire Wire Line
	3950 2200 2950 1900
Wire Wire Line
	4450 2100 5550 1600
Wire Wire Line
	4450 2200 5550 1700
Wire Wire Line
	3950 2700 3000 3250
Wire Wire Line
	3950 2800 3000 3350
Wire Wire Line
	3950 2300 2950 2300
Wire Wire Line
	3950 2400 2950 2400
Wire Wire Line
	4450 2300 5550 2300
Wire Wire Line
	4450 2400 5550 2400
Wire Wire Line
	3950 2500 2950 2750
Wire Wire Line
	3950 2600 2950 2850
Wire Wire Line
	4450 2500 5550 2700
Wire Wire Line
	4450 2600 5550 2800
Wire Wire Line
	3650 4100 5200 4100
Wire Wire Line
	3650 4200 5200 4200
Wire Wire Line
	3650 4600 5200 4600
Wire Wire Line
	3650 4700 5200 4700
Wire Wire Line
	4450 1700 5550 1000
Wire Wire Line
	4450 1800 5550 1100
Wire Wire Line
	4450 1900 5550 1200
Wire Wire Line
	4450 2000 5550 1300
$Comp
L Conn_02x12_Odd_Even RAMPS0
U 1 1 5A5BDB51
P 4150 2200
F 0 "RAMPS0" H 4200 2800 50  0000 C CNN
F 1 "Conn_02x12_Row_Letter_First" H 4200 1500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_2x12_Pitch2.54mm" H 4150 2200 50  0001 C CNN
F 3 "" H 4150 2200 50  0001 C CNN
	1    4150 2200
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 HEATER_0
U 1 1 5A5BB8B9
P 3450 4100
F 0 "HEATER_0" H 3450 4200 50  0000 C CNN
F 1 "Conn_01x02" H 3450 3900 50  0000 C CNN
F 2 "TerminalBlocks_Phoenix:TerminalBlock_Phoenix_MKDS1.5-2pol" H 3450 4100 50  0001 C CNN
F 3 "" H 3450 4100 50  0001 C CNN
	1    3450 4100
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x02 HEATER_1
U 1 1 5A5BB8BF
P 3450 4600
F 0 "HEATER_1" H 3450 4700 50  0000 C CNN
F 1 "Conn_01x02" H 3450 4400 50  0000 C CNN
F 2 "TerminalBlocks_Phoenix:TerminalBlock_Phoenix_MKDS1.5-2pol" H 3450 4600 50  0001 C CNN
F 3 "" H 3450 4600 50  0001 C CNN
	1    3450 4600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3950 1900 3950 1900
Wire Wire Line
	3950 2000 3950 2000
$EndSCHEMATC
