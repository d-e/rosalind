// all length measurements in millimeters

// external X and Y dimensions
external=522;
// external Z dimensions
externalZ = 500;

// size of aluminum profile section
profile_width=40;

// choose frame aluminum profile section
// leave empty for classic T-Slot or provide
// dxf file of section.
// 
// custom_profile="lib/alu_section_40.dxf"; 

// T-Slots available 20x20, 40x40
custom_profile="lib/alu_section_40.dxf"; 
custom_profile_slot_width=8.4;
custom_profile_slot_thickness = 3.65;

// size of T-Slot for the bed platform
bed_profile=20;

// size of the bed platform
bed_platform_length = 300; //length or "auto"
bed_platform_width = 370; //length or "auto"

// cantilever or supported at both ends?
// (also controls number of z motors)
bed_platform_supports = 2; //one or two supported

// size of the heated bed
heated_bed_width = 330;
heated_bed_length = 330;
heated_bed_thickness = 3;

// can we get the glass over the bed mounting screws?
// most beds need a little drilling for that. If you are
// not up to it just set `bed_countersunk_screws` to false
bed_countersunk_screws = true;

// Turn the x-carriage around. Unsupported
// FIXME: when true the belts are not visualized correctly on
flip_secondary_axis=false; 

// extruder drive type, geared or direct. 
// not to be confused with bowden or direct feed
extruder_drive = "direct"; //geared or direct

// frame angle fastener properties
small_angle_fastener_height = 40;
angle_fastener_thickness = 2.5;
frame_angle_fastener_side = 30; 

// smooth rod diameter
y_rod_d = 10;
x_rod_d = 10;
z_rod_d = 8;

// Length of nema motors used
nema_length = 48;
extruder_nema_length = nema_length;

// thickness of acrylic sheet enclosure
acrylic_thickness = 3;

// distance of acrylic sheet enclosure from
// the outer face of the aluminum
// towards the center of the cube.

// for example if you intend to fix the
// acrylic inside the slot, this should be
// equal to profile_width/2+acrylic_thickness/2
acrylic_position = profile_width/4;

mtol=0.5; //bolt tolerance (big)
ctol=0.2; //clamp tolerance (small)

// minimum thickness of any part. 
// 6 makes for a very solid printed machine
minimum_thickness = 7;

// diameter of bolts used to fasten parts on the frame
frame_bolts = 8;

// diameter of bolts used to fasten parts on the bed platform
bed_screws = 3;

// diameter of bolts used to fasten printed parts together (not all parts honor this yet)
part_bolts = 3;

// countersunk bolts for the frame angle fasteners
tapered_angle_fastener_bolts = true;

// frame angle bolt diameter. Defaults to frame bolt diameter
angle_fastener_bolts = frame_bolts;

// diameter of protruding part of
// motor (around the shaft)
motor_shaft_hole = 24;

// motor shaft length
nema_shaft = 21; //22
motor_shaft_diameter = 5;

// leadscrew sizes
leadscrew = [8,8,400];
leadscrew_nut_d = 22;
leadscrew_nut_h = 15;
leadscrew_nut_flange_thickness = 4;
leadscrew_nut_shaft_depth = 9;
leadscrew_nut_shaft_d = 10.2;
leadscrew_bearing = "688";
leadscrew_length = 350; //FIXME auto

// linear bearings 
// FIXME bronze bushings
x_linear_bearing = "LM10LUU";
y_linear_bearing = "LM10UU";
z_linear_bearing = "LM10UU";

// modular x_carriage pcb slot
x_carriage_pcb_width = 60;
x_carriage_pcb_height = 40;
x_carriage_pcb_thickness = 6;

// belt idlers and bearings
y_carriage_bearing = "623";
idler_bearing = "608"; //deprecated
idler_diameter =  12;
idler_height = 8.5;
idler_inner_diameter = 3;
idler_flange_diameter = 16;
idler_flange_thickness = 1;

// motor cogged wheel solid part thickness 
mcwspt = 5;

// Distance from motor zero coord 
// to start of cogged wheel teeth
// (motor zero is where shaft meets motor body)
teeth_offset = 14;

// belt sizes
belt_width = 6;
belt_thickness = 1.36;
belt_pitch = 2;
// distance between the two belts
belt_distance = minimum_thickness;

// bronze bushings 
bushings_d = 11.95;
bushings_h = 9.64;

// bolt size for modular x carriage
modular_x_bolt = 8;

// tensioner thickness and tightening bolt size
tensioner_thickness = 8;
tensioner_screw = 3;

//how much does the extruder protrude below the x-carriage?
extruder_offset = 25;

//endstop stuff
can_flip_endstop = false;
endstop_model = "makerbot";
include <../vitamins/endstop.scad>

//how much is the switch off center?
endstop_offset = 10;
makerbot_endstop = true;

//how much tolerance to leave for the z endstop adjuster screw
endstop_tolerance = 10;

//bed assembly thickness
insulation_thickness = 3; //set to 0 if insulation does not protrude from the bed frame
glass_thickness = 2; //set to 0 if no glass
heated_bed_pcb_thickness = 3;
bed_levelling_tolerance = 5;

// drive gear
extruder_bearing = "608";
drive_gear_hobbed_radius = 6.35 / 2;
drive_gear_hobbed_offset = 3.2;
drive_gear_length = 13;
drive_gear_tooth_depth = 0.2;

// bottom plywood thickness
base_plate_thickness = 10;

// PSU (Power Supply Unit) info
psu_screw_distance = [120, 200];
psu_size = [130, 210, 30];
psu_bolts = 3;

//openscad tolerances
e=0.01;
2e=2*e;

s= 0.01; //set positive to make sure all the parts are separate in the resulting stl
         //set negative to make sure it exports as one monolithic thing

//whether to display vitamins or not
show_vitamins = true;
show_belts = true;
show_rails = true;
show_wood = true;
show_electronics = true;