/**
* This file is a playground for anything that might be needed to be 
* previewed in an assembly and does not specifically fit in another
* file
*
*/
include <general-values.scad>
use <vitamins/aluminium-profile.scad>
use <rosalind.scad>
frame();
/**
* Render the aluminium frame 
*
*/
module frame(){
    translate([0,0,externalZ/2]){
        //Z
        aluminium_profile(externalZ);
        translate([0,external-profile_width+s,0]) aluminium_profile(externalZ);
        translate([external-profile_width+s,external-profile_width+s,0]) aluminium_profile(externalZ);
        translate([external-profile_width+s,0,0]) aluminium_profile(externalZ);
    }
    translate([(external)/2,0,externalZ]) rotate([0,90,0]){
        //X
        XYframe();
    }
    XYangles();
    verticalAngles();
    translate([(external),(external)/2,externalZ]) rotate([0,90,90]){
        //Y
        XYframe();
    }
    translate([0,0,externalZ-profile_width]){
        XYangles();
    }
}
