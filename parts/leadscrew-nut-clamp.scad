/**
* Leadscrew brass nut pillow block. The part that 
* mounts the bed on the leadscrew so that it moves
* when the Z-motor turns.
*
*/

include <../general-values.scad>
use <../lib/mechanical.scad>
use <z-axis-clamp.scad>
use <../lib/utl.NEMA.scad>
use <../lib/nutsnbolts/cyl_head_bolt.scad>
use <../assemblies/bed.scad>
use <bed-clamps.scad>
include <../lib/colors.scad>

/* Constants */

thicker = 3;

/* Print or Preview */

print();
// leadscrew_nut_clamp_internal(with_nut=true);

/* Calculated Values */

/**
* Structural thickness: A thickness that allows some 
* structural integrity to the printed material 
*/
sthick = minimum_thickness; 
length = (external - profile_width*2 - z_axis_clamp_x_mountpoint_offset()*2 - bed_size()[1])/2;

height = bed_profile;
width = bed_profile*2;

bolts = bed_screws;

/* Modules */

/**
* Render the nut clamp in an orientation best for 3D printing 
*
*/
module print(){
    leadscrew_nut_clamp();
    if (bed_platform_supports==2) translate([width*2,0,0]) leadscrew_nut_clamp(); 
}

/**
* Render the nut clamp laid out best for the display in the complete
* Rosalind assembly
*
* @param with_nut bool Whether to print the nut itself or not.
*/
module leadscrew_nut_clamp(with_nut = false){
    translate([0,0,-bed_profile/2]) leadscrew_nut_clamp_internal(with_nut = with_nut);
}

/**
* Render the nut clamp laid out best for development
*
*/
module leadscrew_nut_clamp_internal(with_nut = false){
    half_nema = nema17_size(nema_length)[0]/2;
    mv = [0,-(external-2*profile_width-bed_size()[1]-nema17_size(nema_length)[0])/2,0];
    difference(){
        translate(mv) union() {
            rounded_corner(length);
            mirror([1,0,0]) rounded_corner(length);;
            translate([-width/4,0,0]) cube([width/2,length,height]);
            translate([-width/2,0,0]) cube([width,length/2,height]);
        }
        screw_nut_hole();
        screws();
    }
    if (with_nut) screw_nut();
}

/**
* Return the size of the nut clamp (leadscrew pillow block)
*
* @return array3 XYZ dimensions of the nut clamp
*/
function leadscrew_nut_clamp_size() = [width,length*2,height];


/**
* Print the screw holes (this module is to be subtracted)
*
*/
module screws(){
    mirror([1,0,0]) screw();
    screw();
}

/**
* Print a screw hole of length (This module is to be subtracted
* to make holes as it does not bother with threads)
*
* @param screw_length float The length of the screw
*
*/
module screw(screw_length = 100){
    y = length/2;
    translate([width/3,y,height/2]) rotate([90,0,0]) {
        translate([0,0,0]) cylinder(d=bolts+mtol, h=screw_length);
        translate([0,0,-screw_length+e]) cylinder(d=2*bolts+mtol, h=screw_length);
    }   
}

/**
* Hole that fits the leadscrew nut (To be subtracted)
*
* @param tolerance float How bigger should the hole should be 
*                        so that it fits the nut easily 
*/
module screw_nut_hole(tolerance = 1){
    translate([0,0,-e]) cylinder(d=leadscrew_nut_shaft_d+tolerance, h=bed_profile+2e);
}

/**
* Leadscrew nut with flange
*
* @param tolerance float How much bigger should this be rendered 
*
*/
module screw_nut(tolerance = 1){
    color(vitamins) translate([0,0,-1.30 -3.6]) {
        difference(){
            union(){
                cylinder(d=10.2+tolerance, h=14.90);
                translate([0,0,1.30]) cylinder(d=21.82+tolerance, h=3.60);
            }
            translate([0,0,-e]) cylinder(d=8.1, h=20);
        }
    }
}