/**
* Mounts for the z-axis smooth rods on the frame.
* Includes mounting space for the z-axis endstop switch
*
*/
include <../general-values.scad>
include <../config/calculated-values.scad>
use <../lib/nutsnbolts/cyl_head_bolt.scad>
use <../lib/mechanical.scad>
use <../lib/utl.NEMA.scad>
use <bed-clamps.scad>

/* Constants */
save_material = true;

/* Print or preview */

// z_endstop_extension();
print_all_z_axis_clamps();

// print_spacer();
// annotate();
// z_axis_clamp(endstop=true, print = false);
// z_axis_clamp_internal(endstop = true);
// rotate([0,90,0]) endstop_thickener();

/* Calculated Values */
thickness = minimum_thickness;
// extra offset so that the belt doesn't touch the z rods
extra_offset = y_axis_clamp_y_min()-y_axis_clamp_idler_offset()[2]-idler_diameter/2+belt_thickness*2-thickness;

rod = z_rod_d;
bolt = frame_bolts;
_height = bolt+8;

fastener_width=min(_height*3+bolt*2, _bed_platform_length/2-bed_profile*2-2e);
rodt = rod + mtol/2;
boltt = bolt+mtol;
fins = rod*1.5;

/**
* Render some spacers in case you need more extra offset and you don't want 
* to reprint
*
* @param t float how thick is the spacer
*
*/
module print_spacer(t=belt_thickness){
    intersection(){
        union(){
            print();
            translate([-3*_height-30, 0, 0]) print();
            translate([-2*_height-20, 0, 0]) print();
            translate([-_height-10, 0, 0]) print();
        }
        translate([-500,-500,0]) cube([1000,1000,t]);
    }
}


/**
* Render some textual annotations for the wiki
*
*
*/
module annotate(){
    translate([20, 20, 0]) {
        rotate(90) annotext("z-axis clamp", halign="right");
        translate([20, 0, 0]) rotate(90) annotext("with endstop", halign="right");
    }
    translate([20, 30, 0]) rotate(90) annotext("z-axis clamps");
    translate([-145, 30, 0]) rotate(90) annotext("leadscrew");
    translate([-125, 100, 0]) rotate(90) annotext("pillow");
    translate([-105, 100, 0]) rotate(90) annotext("block");
}


/**
* Render all required z-axis-clamps for printing
* Includes leadscrew clamp
*
*/
module print_all_z_axis_clamps(){
    print(endstop=true, thickener = false);
    translate([-_height-10, fastener_width + 10, 0])  print(thickener = false);
    translate([-2*_height-20, fastener_width + 10, 0])  print(thickener = false);
    translate([0, fastener_width + 10, 0]){
        for(i=[0:bed_platform_supports*2-2]){
            translate([-profile_width*i,0,0]) print(); 
        } 
    } 
    translate([-5*profile_width,fastener_width+10,_height]) rotate([180,0,0]) z_axis_screw_clamp(z_axis_screw_clamp_size,save_material=true);
    z_endstop_extension();
}

/**
* Render the z axis clamp laid out for easy 3D printing
*
* @param endstop  boolean   Whether to include extension and endstop switch mount.
*                          Only one of the z-axis clamps has an endstop mount.
*
*/
module print(endstop = false, thickener = false){
    rotate ([0,-90,0]) translate([thickness+extra_offset+rodt/2,0,0]) z_axis_clamp(endstop, print = true);
    if (thickener) translate([10,30,0]) rotate([0,90,0]) endstop_thickener();
}

/**
* Render the part used to mount the leadscew to the frame
* in a way that makes it easier to print
*
* @param height The height of the clamp
*
*/
module print_leadscrew_clamp(height, pillow_thickness){
    rotate([180,0,0]) z_axis_screw_clamp(height, pillow_thickness);
}

/**
* The part that mounts the vertical (z-axis) smooth rods
* to the printer's frame
*
* @param endstop bool   Whether to include extension and endstop switch mount.
*                       Only one of the z-axis clamps has an endstop mount.
*
*/
module z_axis_clamp(endstop = false, print = true){
    z_axis_clamp_internal(endstop, print = print);
}

/**
* The part that mounts the vertical (z-axis) smooth rods
* to the printer's frame. Internal implementation rotated 
* in a way that assists development.
*
* @param endstop bool   Whether to include extension and endstop switch mount.
*                       Only one of the z-axis clamps has an endstop mount.
*
*/
module z_axis_clamp_internal(endstop = false, print = true){
    ext_d = (rodt+thickness*2);
    cylinder_height = endstop?externalZ-z_home-profile_width/2+_height/2-endstop_tolerance:_height;
    difference(){
        //clamp
        union() {
            //fins
            translate([rodt/2,-3*thickness/2,0]) cube([fins,thickness,_height]);
            translate([rodt/2,1*thickness/2,0]) cube([fins,thickness,_height]);
            difference(){ 
                hull(){
                    cylinder(d=ext_d, h=cylinder_height);
                    if (endstop) translate([-ext_d/2-extra_offset,-ext_d/2,0]) cube([thickness+extra_offset,ext_d,cylinder_height]);
                }
                translate([-ext_d/2-extra_offset-e,-ext_d,y_axis_clamp_z_offset-y_axis_clamp_idler_offset()[1]-rodt/2-belt_width*2.25])
                     cube([extra_offset+thickness,ext_d*2,belt_width*4]);
            }
            if (endstop){
                translate([-ext_d/2-extra_offset,endstop_width-endstop_offset,cylinder_height-endstop_height]) {
                    rotationz = endstop_model=="makerbot" ? 90 : -90;
                    if (!print) translate([-endstop_thickness,0,0]) rotate([180,90,0]) rotate([0,0,90]) endstop(endstop_model);
                    translate([0,-endstop_width,0]) {
                        cube([5,endstop_width,endstop_height]);
                        if (!print) endstop_thickener();
                    }   
                }
            }
        }
        //pocket
        translate([0,-thickness/2,-e]) cube([thickness*10,thickness,cylinder_height+2e]);
        translate([0, 0, -e]) cylinder(d=rodt, h=cylinder_height+2e);
        //screw
        h=(thickness*2+rodt)*2;
        translate([rodt/2+fins/2,h/2,_height/2]) rotate([90,0,0]) cylinder(d=3+mtol, h=h);
        
        //nut & head traps
        translate([rodt/2+fins/2,thickness,_height/2]) rotate([90,0,0]) nutcatch_parallel("M3", l=3, clk=0.1);
        translate([rodt/2+fins/2,-thickness,_height/2]) rotate([90,0,0]) cylinder(d=6, h=3);
    }
    plate();
}

/**
* The mounting plate, with holes for the frame-mounting screws
*
* @param offset float The distance between the axis of the rod and the
*                     surface of the frame
*
*/
module plate(offset=rodt/2+thickness+extra_offset){
    difference(){
        translate([-offset,-fastener_width/2,0]) cube([thickness+extra_offset, fastener_width, _height]);
        //screw
        translate([-offset-e,fastener_width/4+thickness/2+bolt/3,_height/2]) rotate([0,90,0]) cylinder(d=boltt, h=thickness+extra_offset+2e);
        translate([-offset-e,-fastener_width/4-thickness/2-bolt/3,_height/2]) rotate([0,90,0]) cylinder(d=boltt, h=thickness+extra_offset+2e);
    }
}

/**
* The part that holds the upper end of the leadscrew in place, via bearing
*
* @param height float The height of the part
* @param pillow_thickness float The thickness of the plastic around the bearing
* @param save_material bool Whether to fill the hole above the end of the leadscrew
*                           with plastic to increase stiffness or not, to conserve
*                           filament
*
*/
module z_axis_screw_clamp(height=_height, pillow_thickness=3, save_material=false){
    distance = nema17_size(nema_length)[0]/2; //distance from plate face to center of leadscrew
    plate(distance);
    bearing = bearing_size(leadscrew_bearing);
    bod = bearing[1];
    bh = bearing[2];
    height_diff = _height-height;
    difference(){
        union(){
            translate([0,0,height_diff]) cylinder(d=bod+pillow_thickness,h=height);
            translate([-distance,-(bod+pillow_thickness)/2,0]) cube([distance, bod+pillow_thickness, _height]);
            // translate([-bod/2,0,_height/2-height_diff]) cube([bod, bod+pillow_thickness, _height], center=true);
        }
        translate([0, 0, height_diff-e]) cylinder(d=bod+0.5,h=(save_material?height:bh)+2e);
    }
}

/**
* The mounting plate of the endstop switch might need to be thicker than the rest
* of the clamp in order for the adjustment screw to squarely hit it. However including
* this in the design itself would make the part unprintable without supports so we
* will print it separately and glue it together.
*
*/
module endstop_thickener(){
    difference(){
        translate([-endstop_thickness,0,0]) cube([endstop_thickness,endstop_width,endstop_height]);
        // %translate([-endstop_thickness,0,0]) rotate([180,90,0]) rotate([0,0,90]) endstop(endstop_model);
        if (endstop_model == "makerbot") rotate([0,90,0]) {
            translate([-3,-3,-15]) cylinder(d=4,h=30);
            translate([-14,-17,-15]) cylinder(d=4,h=30);
            translate([-14,-3,-15]) cylinder(d=4,h=30);
            translate([-14,-36,-15]) cylinder(d=4,h=30);
        }
    }
}

module z_endstop_extension(){
    translate([-140,0,0]) cube([16,20,2]);
}

/* functions */

/**
* The size of the z-axis smooth rod mounting clamp
*
* @return array3 The x, y and z dimensions of the part
*/
function z_axis_clamp_size() = [thickness+rodt+fins+extra_offset,fastener_width,_height];

/**
* The distance between the z-axis smooth rod center and the surface
* of the frame. 
*
* @return float The distance between the rod axis and the frame
*/
function z_axis_clamp_x_mountpoint_offset() = rodt/2+thickness+extra_offset;
