/**
* The part that mounts the x-axis on the y-axis
* Otherwise known as the y-carriage
*
*/
use <../lib/mechanical.scad>
include <../general-values.scad>
use <../lib/nutsnbolts/cyl_head_bolt.scad>
use <x-carriage.scad>
use <../lib/functions.scad>
use <../vitamins/belt.scad>
use <y-axis-clamp.scad>
include <../config/calculated-values-2.scad>

/* Constants */

rail_tolerance = 0.2;
lpl = 55-e;

clamp_screws = 3;
lmuPos = 20;
lmuZ = 0;
clamp_slot_height = 2;

bearingClampTolerance = btol;

/* Print or Preview */

//print_all_x_axis_clamps();
// translate([100,0,0]) print("right");
// x_axis_clamp(false, side="right", position=80);
 translate ([0,100,0]) x_axis_clamp(false, side="left", position=80);

/* Calculated Values */

sthick = ceil(minimum_thickness/2); // structural thickness
bearingHeight = bearing_size(y_carriage_bearing)[2];
bearingOD = bearing_size(y_carriage_bearing)[1];
bearingID = bearing_size(y_carriage_bearing)[0];
nuth = 5;

lpw = bearingOD+profile_width/2; //left part width
lmuHeight = bearing_size(y_linear_bearing)[1];
x_rod_dist = x_carriage_rod_distance;
bearing2Z = sthick+mtol+bearingHeight+mtol+sthick+mtol;
screw_end = bearingHeight+2*mtol+sthick+sthick+nuth;
screw_end2 = bearing2Z+bearingHeight+sthick+2*mtol;
alurodZ = x_carriage_rod_distance/2;
lph = lmuHeight+5*sthick; //violating the thickness 
                          //rule so that the belt stays horizontal

alurodY = lpl-x_rod_d-sthick;
bottom = alurodZ-x_rod_d-x_rod_dist;

clamp_bolts = part_bolts; 
clamp_bolt_head = part_bolts*2; 

clamp_width = lpl-alurodY+x_rod_d/2+sthick;

/* Modules */

/**
* Print both x-axis clamps
*
*/
module print_all_x_axis_clamps(){
    print(side="left", clamp=true);
    translate([0,-10,0]) print(side="right", clamp=true);
}

/**
* Lay out the x-axis clamps for 3D printing 
*
* @param side     string    left: print the left x-axis clamp
*                           right: print the right x-axis clamp
* @param clamp    bool      Whether to print the smaller part that 
*                           actuall clamps on the bearing too.
* @param body     bool      Whether to print the bigger part (body) 
*                           of the carriage
*
*/
module print(side="left", clamp=true, body=true){
    mi = side=="left"?[0,0,0]:[0,1,0];
    mirror(mi){
        if (body) translate([0,0,lpw]) rotate([90,90,90]) body(side, print=true);
        if (clamp) translate([20,60,lpw]) rotate([0,-90,90]) translate([-lpw,0,-screw_end2]) clamp();
    }
}

/**
* Render the x-axis clamp (y-carriage) in a way that it can 
* be previewed in the 3D printer
*
* @param original_coords bool   Whether to render it in its original coordinates
*                               for easy development or centered on the y-axis
*                               for easier assembly
* @param side            string left: print the left x-axis clamp
*                               right: print the right x-axis clamp
* @param position        float  The position of the carriage along the y-axis
*
*/
module x_axis_clamp(original_coords=false, side="left", position = (external-2*profile_width)/2){
    mi = side=="left"?[1,0,0]:[0,0,0];
    trans2 = side=="left"?[-lpw,0,0]:[0,0,0];
    trans = original_coords?[0,0,0]:[lpw/2,alurodY,0];
    rot = original_coords?[0,0,0]:[0,0,180];
    translate(trans) rotate(rot){
        mirror(mi) translate(trans2){
            body(side, position = position);
            translate([-1,0,0]) clamp();
        }
        x_rails_positioned(0,side=side);
    }
    //difference between part length and bearing length. We need half of it so that the bearing
    //is positioned in the center of the part
    halfdiff = (lpl-bearing_size(x_linear_bearing)[2])/2;
    //the position of 0,0 relative to the start of the part
    zero = alurodY-lpl;
    //the bearing
    color("silver") translate([0,zero+halfdiff,lmuZ]) rotate([-90,0,0]) bearing(name=x_linear_bearing);
}

/**
* The shelves where the timing belt idlers sit, below the body.
*
* @param tolerance float How much space (height) to give to the idlers 
*                        so that they move freely
*/
module idler_shelves(tolerance=1.2){
    shelves_h = belt_width+belt_distance;
    shelves_w = idler_flange_diameter;
    shelves_l = lpl-clamp_width+minimum_thickness/2;
    translate([lpw-shelves_w,0,-lph/2+tolerance/2-2*shelves_h]) {
        // top block
        translate([0,0,shelves_h]) cube([shelves_w/2, 10, shelves_h]);
        // bottom blocks
        cube([shelves_w, 10, shelves_h]);
        translate([0,shelves_l-minimum_thickness/2,0]) cube([shelves_w, minimum_thickness/2, shelves_h*2]);
        // top shelf
        translate([0,0,shelves_h]) cube([shelves_w, shelves_l, shelves_h-idler_height-tolerance]); //TODO: check height
        // bottom shelf
        cube([shelves_w, shelves_l, shelves_h-idler_height-tolerance]); //TODO: check height
    }
}

/**
* The body (larger part) of the x-axis clamp (y carriage)
*
* @param side            string left: print the left x-axis clamp
*                               right: print the right x-axis clamp
* @param position        float  The position of the carriage along the y-axis
*
*/
module body(side="left", position = (external-2*profile_width)/2, print = false){
    x = x_axis_clamp_idler_center()[0];
    notchY = x_axis_clamp_idler_center()[1];
    sig = side=="right"?1:-1;
    echo (str("Idler Diameter: ",idler_diameter/2));
    tyzero = notchY+idler_diameter/2-alurodY;
    tensionerY= tyzero+x_carriage_size()[1]/2+tensioner_block_cap_size()[1]+tensioner_thickness;
    difference(){
        union(){
            whole();
            idler_shelves();
        }
        clamp_cut(tolerance=0.2);        
        //bearing screw notch
        notchH = idler_height+minimum_thickness*2;
        transz = -(lph/2+0.1+idler_height+minimum_thickness);
        shelves_h = +belt_width+belt_distance;
        translate([x,notchY,transz]) { 
            translate([0,0,-shelves_h]) cylinder(d=bearingID, h=notchH+shelves_h, $fn=16);
            translate([0,0,notchH]) nutcatch_sidecut(str("M",bearingID),x);
        }
    }
    // timing belt idlers and bearings
    translate([x,notchY,-lph/2-0.1]) { 
        vitamin(!print) if (side=="left"){
            translate([0,0,-bearingHeight]) {
                bearing(name="623", flanged=2);
                translate([0,0,-bearingHeight]) bearing(name="623", flanged=1);
            }
            translate([0,0,-idler_height-belt_width-belt_distance]) idler(idler_diameter=idler_diameter, idler_height=idler_height);
        } else {
            translate([0,0,-idler_height]) idler(idler_diameter=idler_diameter, idler_height=idler_height);
            translate([0,0,-bearingHeight-belt_width-belt_distance]) {
                bearing(name="623", flanged=2);
                translate([0,0,-bearingHeight]) bearing(name="623", flanged=1);
            }
        }
        position=side=="left"?position-15:external-2*profile_width-position-15;
        //belt
        belt_length = position-x_carriage_size()[0]/2+lpw/2;
        translate([0,bearingOD/2+belt_thickness,-(belt_width+1)]){
            d = -tensionerY;
            beltstart_up= side=="right"?[0,-idler_diameter]:[0,0];
            beltstart_down= side=="left"?[0,-idler_diameter]:[0,0];
            belt([beltstart_up,[-belt_length,d]], show_belts=show_belts&&!print);
            translate([0,0,-belt_width-belt_distance]) belt([beltstart_down,[-belt_length,d]], show_belts=show_belts&&!print);
        }
    }
    if(side=="left") translate([endstop_height,alurodY-y_rod_d/2-sthick-endstop_width,lph/2]) rotate(90) endstop(endstop_model, show=show_vitamins&&!print);
}

/**
* The two pieces of the bearing clamp are produced by intersecting
* a cutting cube with the single piece item. This module returns 
* the smaller part, the cap if you want.
*
*/
module clamp(){
    intersection(){
        whole();
        clamp_cut(tolerance=-0.5);
    }
}

/**
* This is the part that contains the linear bearing
* It does not include the idler shelves and it is a 
* single piece, meaning that you would need to jam 
* the bearing inside. It is produced by mirroring the
* half part below.
*
*/
module whole(){
    mirror([0,0,1]) half(); 
    half();
}

/**
* The body of the y-carriage is symmetrical so it 
* makes sense to only model the upper (or lower) 
* half and then mirror it. This module does exactly
* that.
*
* @param print   bool   Whether to lay the item out
*                       for printing or preview. 
*                       (Controls orientation and 
*                       rendering of vitamins etc.) 
*/
module half(print = false){
    screw_head_height = 0;
    rod_ear_base_height = alurodZ-lph/2;
    difference() {
        nut_h=5;
        top_of_clamp_screw = alurodZ+7;
        vch=x_rod_d*2;
        screw_l=max(x_rod_d,10);
        
        fortification_height = 6;
        union(){
            cube([lpw,lpl,lph/2]);
            //rod clamps
            hull(){
                t=clamp_width;
                translate([0,lpl-t,lph/2]) cube([lpw,t,rod_ear_base_height]);
                d=x_rod_d+sthick*2;
                translate([0,alurodY,lph/2+rod_ear_base_height]) rotate([0,90,0]) cylinder(d=d, h=lpw);
            }
            //upper rail clamp stiffener
            translate([-e,alurodY+x_rod_d/2,alurodZ+clamp_slot_height]) cube([lpw+2e,lpl-alurodY-x_rod_d/2,clamp_slot_height]);
            //vertical clamp screw support
            //translate([lpw/2,lpl-(clamp_bolts+sthick)/2,alurodZ+clamp_slot_height]) cylinder(d=clamp_bolts+sthick, h=fortification_height);
        }
        screw_l2 = max(ceil5(screw_l),10);
        //vertical clamp screw holes
        translate([lpw/2,lpl-(clamp_bolts+sthick)/2,alurodZ+clamp_slot_height*2-screw_l2+e]) {
        //replace with         
        //translate([lpw/2,lpl-(clamp_bolts+sthick)/2,alurodZ+vch-25]) { //25mm screws
            //tightening screw hole
            cylinder(d=clamp_bolts, h=ceil5(screw_l2+e), $fn=16);
            // echo(str("Screw M3x",screw_l2));
            //space for tightening-screw head
            translate([0,0,screw_l2-screw_head_height]) cylinder(d=clamp_bolt_head, h=13);    
            //nut traps 
            rotate([0,0,90]) translate([0,0,minimum_thickness/2]) nutcatch_sidecut(str("M",clamp_bolts), l=10, clh=0.2, clsl=0.2);
        }
        
        // LMXUU bearing cutout
        translate([lpw/2,-e,lmuZ]) rotate([-90,0,0]) cylinder(d=bearing_size(x_linear_bearing)[1]+bearingClampTolerance, h=lpl+2e, $fn=64);
        
        translate([0,0,lmuHeight/2+sthick]) clamp_screws();
        
        //upper rail clamp slot
        translate([-e,alurodY,alurodZ]) cube([lpw+2e,lpl-alurodY,clamp_slot_height]);

        //rod support
        x_rail_stubs(rail_tolerance);
    }
}

/**
 * x-rail stubs for subtracting the holes
 *
 * @param rail_tolerance float  How much bigger to render
 *                              the rails so that the holes
 *                              produced by subtracting them 
 *                              fit the actual rails easily
 * @param side           string left: the rails go from here 
 *                                    and to the right (X+)
 *                              right:the rails go from here
 *                                    and to the left (X-)
 */
module x_rail_stubs(rail_tolerance=0, side){
    translate([-1,alurodY,0]) x_rails(rail_tolerance);  
}

/**
* x-rails for display in the final assembly
*
* @param rail_tolerance float  How much bigger to render
*                              the rails so that the holes
*                              produced by subtracting them 
*                              fit the actual rails easily
* @param side           string left: the rails go from here 
*                                    and to the right (X+)
*                              right:the rails go from here
*                                    and to the left (X-)
*/
module x_rails_positioned(rail_tolerance=0, side){
    if (side=="left") rail() translate([0,alurodY,0]) x_rails(rail_tolerance);   
}

/**
* render a single x-rail
*
* @param rail_tolerance float  How much bigger to render
*                              the rail so that the holes
*                              produced by subtracting them 
*                              fit the actual rails easily
*/
module x_rail(tolerance=0){
    length = external-y_axis_clamp_x_offset()*2-profile_width*2+lpw; //lpw is the width of the whole clamp
    translate([0,0,alurodZ]) rotate([0,90,0]) cylinder(d=x_rod_d+tolerance, h=length, $fn = 64); 
}

/**
* render both x-rails (just mirrors the single x-rail)
*
* @param tolerance float  How much bigger to render
*                         the rails so that the holes
*                         produced by subtracting them 
*                         fit the actual rails easily
*/
module x_rails(tolerance=0){
    mirror([0,0,1]) x_rail(tolerance); x_rail(tolerance);
}

/**
* Render the space for the screws that attach the clamp to the body
* (this module does not render threads as it is intended for subtraction)
*
*/
module clamp_screws(){
        //clamp screws
        translate([-mtol,10,0]) rotate([0,90,0]) cylinder(d=clamp_screws+mtol, h=lpw+2*mtol);
        translate([-mtol,lpl-10,0]) rotate([0,90,0]) cylinder(d=clamp_screws+mtol, h=lpw+2*mtol);
        //nut traps
        m3_h=2;
        translate([m3_h,10,0]) rotate([0,90,0]) nutcatch_parallel("M3", l=m3_h+e);
        translate([m3_h,lpl-10,0]) rotate([0,90,0]) nutcatch_parallel("M3", l=m3_h+e);
        //head cones
        translate([lpw-m3_h,10,0]) rotate([0,90,0]) cylinder(d=5+mtol, h=m3_h);
        translate([lpw-m3_h,lpl-10,0]) rotate([0,90,0]) cylinder(d=5+mtol, h=m3_h);
}

/**
* The cube used to intersect the `whole()` in order to get two parts
* that press together to clamp down on the linear bearing
*
* @param tolerance float If we cut exactly then the two parts probably won't 
*                        fit together due to printer inacuracy. Negative tolerance is 
*                        used to produce a smaller part when getting the intersection
*                        (clamp) than when subtracting (body/positive tolerance) so
*                        that everything fits nicely.
*
*/
module clamp_cut(tolerance=0){
    // I could have just made it equal in height 
    // with the clamp (lmuHeight+5*sthick) but 
    // this way assembly is easier (there's a slot so you don't need 
    // to hold the linear bearing while you screw the clamp
    translate([-e,-e,-lmuHeight/2-sthick*2-tolerance/2]) cube([lpw/2,lpl+2e,lmuHeight+4*sthick+tolerance]); //other part
}

echo("alurodY",alurodY);

/**
* DEPRECATED: Return the total height of the part without idlers
* Replaced by `x_axis_clamp_size()[2]`
*
* @return float The height of the clamp excluding idlers
*/
function x_axis_clamp_height() = lph;

/**
* DEPRECATED: Return the total width of the part without idlers
* Replaced by `x_axis_clamp_size()[1]`
*
* @return float The width of the clamp excluding idlers
*/
function x_axis_clamp_width() = lpw; 

/**
* Return the total size of the part without idlers
*
* @return array3 The x,y,z dimensions of the clamp excluding idlers
*/
function x_axis_clamp_size() = [lpl,lpw,lph]; 

/**
* Return the width of the clamping part (cap) of the y-carriage
*
* @return float The width of the clamping part (cap) of the y-carriage
*/
function x_axis_clamp_only() = clamp_width;

/**
* Returns the xy position of the mounting hole of the timing 
* belt idlers below the x-axis clamp (y-axis carriage)
*
* @return array2 x,y coordinates of the position of the idlers
*/
function x_axis_clamp_idler_center() = [lpw-idler_diameter/3,lpl-clamp_width-idler_diameter];

/**
* Returns the Y coordinate of the center of the x-rails 
* relative to the carriage. In global terms the part is 
* centered at the center of the rais so this number is what
* you need to reach it's end towards the global 0.
*
* @return float The distance between the beginning of the 
*               x-axis clamp and the center of the x-rails
*               along the y-axis.
*/
function x_axis_clamp_y_offset() = flip_secondary_axis?lpl-alurodY:alurodY;

/**
* Return the length of the x-rails
*
* @return float The length of a single x-axis smooth rod
*/
function x_rod_length() = external-y_axis_clamp_x_offset()*2-profile_width*2+lpw;