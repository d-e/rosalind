/**
* The part that moves along the x-axis and carries the extruder
*
*/
include <../general-values.scad>
use <../lib/mechanical.scad>
use <../lib/nutsnbolts/cyl_head_bolt.scad>
use <x-axis-clamp.scad>
include <../config/calculated-values-2.scad> 
use <../lib/functions.scad>

/* Print or Preview */

print();
// x_carriage(false,true);
// tensioner(nut = true);

/* Constants */

/**
* Unsupported: Turn this on to print a single-piece x-carriage
* on which the x-rails slide directly instead of using bearings
* This could prove to allow faster speeds due to less carriage 
* weight
*/
onepiece = false;
/**
* Unsupported: Turn this off to replace the linear bearings with 
* bronze bushings, in order to keep the weight down.
*/
lmu = true;
tensioner_block_cap_thickness = 3;
$fn = 64;

/* Calculated Values */

sthick = max(minimum_thickness,x_rod_d*0.75);
length = bearing_size(x_linear_bearing)[2];

echo(str("Length: ",length));

screws = part_bolts;
screwst = screws + mtol;

_wwide = x_rod_d+2*sthick-screws;
tensioner_block_thickness = max(_wwide/2,3*belt_thickness+2*part_bolts) + tensioner_block_cap_thickness;
_w = onepiece?x_rod_d+sthick-screws:_wwide;

width = ceil5(_w)+screws; //round to nearest 5mm so that screws don't protrude
echo(str("Width: ",width));
echo(str("Height: ",height));

_height = x_carriage_rod_distance;

tensioner_slot_thickness = belt_thickness*1.3;
tensioner_slot_width = belt_width*1.3;

height = max(x_carriage_rod_distance+_wwide+2*sthick, 2*(tensioner_slot_width+x_axis_clamp_height()/2+belt_distance+belt_distance-0.5*(tensioner_slot_width-belt_width)+tensioner_slot_width)+sthick);

if (tensioner_thickness>width/2) echo("<font color=red><b>ERROR:</b> x-carriage too thin</font>");


/* Modules */

/**
* Render the x-carriage laid out for preview in the general assembly
*
* @param rails    bool Whether to display the x-rails
* @param bearings bool Whether to display the linear bearings
*
*/
module x_carriage(rails=true, bearings=false){
    mi = flip_secondary_axis?[0,1,0]:[0,0,0];
    rotate(180) translate([-length/2,0,0]) mirror(mi){
        front();
        back();    
        tensioner_block();
        tensioner_block_cap();
        if (rails) %translate([120,0,0]) x_rails();
//        if (!onepiece) mirror([0,1,0]) pcb_slot();
        if (bearings) {
            translate([0,0,_height/2]) rotate([0,90,0]) bearing(name=x_linear_bearing);
            translate([0,0,-_height/2]) rotate([0,90,0]) bearing(name=x_linear_bearing);
        }
    }
}

/**
* Render the x-carriage laid out for easy printing without supports
*
*/
module print(){
    if (onepiece){
        rotate([0,270,0]) whole();
        rotate([90,0,0]) translate([-length-10,(_wwide+width)/2,-20]) {
            tensioner_block();
            translate([100,0,0]) tensioner_block_cap();
        }
    } else {
        translate([0, 0, width/2]) rotate([90,0,0]) front();
        translate([length+20,0,width/2]) rotate([-90,0,0]) back();
//        translate([-length-10, 10, -width]) rotate([90,0,0]) pcb_slot();
        translate([-length-10,0,tensioner_block_thickness+width/2]) rotate([90,0,0]) tensioner_block();
        translate([0,55,width/2+tensioner_block_cap_thickness]) rotate([90,0,0]) tensioner_block_cap();
    }
    
}

/**
* The x-carriage is modelled as a whole and then cut into two parts
* this is the "back", i.e. the part away from the extruder
*
*/
module back(){
    intersection() {
        whole();
        cut();
    }
}

/**
* The "front" i.e. the part nearest the extruder
*
*/
module front(){
    difference() {
        whole();
        cut();
    }
}

/**
* The x-carriage is modelled as a whole and then cut into parts
* This is the cube that is used to cut these parts
*
*/
module cut(){
    translate([-e,-e,-height/2-e]) cube([length+2*e, width/2+2*e, height+2*e]);
}

/**
* The x-carriage as a whole. This module will then be cut into 
* two parts which are printed and then screwed together, sandwiching
* the bearings/bushings
*
*/
module whole(){
    difference() { 
        translate([0,-width/2,-height/2]) cube([length,width,height]);
        // this is here in order for the lmu=false option to work
        translate([-e,0,0]) x_rails(mtol*2);
        
        //Lower rail space is bigger so that the carriage moves freely even if 
        //aluminium rods are not completely parallel
        
        //bushing tolerance is also bigger, see `bushings()`
        translate([120,0,0]) {
            x_rail(onepiece?0.3:mtol*3);
            mirror([0,0,1]) x_rail(onepiece?0.35:mtol*2); 
        }
        
        screws();
        if (!onepiece) 
            if (lmu){
                //Lower rail space is bigger so that the carriage moves freely even if 
                //aluminium rods are not completely parallel
                translate([-e,0,_height/2]) rotate([0,90,0]) bearing(name=x_linear_bearing, hull=true, tolerance=btol, h=length+2e);
                translate([-e,0,-_height/2]) rotate([0,90,0]) bearing(name=x_linear_bearing, hull=true, tolerance=btol*2, h=length+2e);
            }else
                //bushing tolerance is also bigger, see `bushings()`
                bushings(mtol);
        
        //positioning spheres
        positioning_spheres(false);
        
        //single_bolt
        translate([length/2, width/2+e, 0]) rotate([90,0,0]) cylinder(d=modular_x_bolt+mtol, h=width+2e);
        translate([0,-tensioner_block_cap_thickness,0]) cap_screws();
    }
    if(onepiece) mirror([0,1,0]) pcb_slot();
    //if (flip_secondary_axis) endstop_extension();
    
}
//positioning_spheres(true);

/**
* Spheres that fit the modular x-carriage tool attachment. These spheres
* allow the tool attachment to attach with a single bolt, by fitting into
* the corresponding recesses on the carriage and securing the attachment 
* into place.
*
* @param centered bool Applies a translation similar to what the `center` 
*                      argument of the cube primitive does.
* @param tolerance float How much bigger or smaller the spheres should be
*                        rendered. Used to compensate for printer 
*                        inacuracy. (Larger recess (+), Smaller spheres (-)) 
*
*/
module positioning_spheres(centered=true, tolerance=0){
    trans=centered?[-length/2, -width/2+e, 0]:0;
    translate(trans){
        translate([0,0,2*height/5]) two_spheres(tolerance=tolerance);
        two_spheres(tolerance=tolerance);
    }
    module two_spheres(tolerance=0){
        translate([length/4,width/2,-height/5]) sphere(d=modular_x_bolt+tolerance);
        translate([3*length/4,width/2,-height/5]) sphere(d=modular_x_bolt+tolerance);
    }
}

/**
* The tensioner block is an attachment to the x-carriage that holds the 
* ends of the belts in a configuration for easy belt tensioning by just
* turning two screws. Module translated for easy assembly.
*
*/
module tensioner_block(){
    intersection(){
        translate([0,-width/2-tensioner_block_thickness,-height/2]) cube([length,tensioner_block_thickness-tensioner_block_cap_thickness,height*.45]);
        _tensioner_block();
    }
}

/**
* Internal representation of the tensioner block laid out for easier
* development. See `tensioner_block()` for more.
*
*/
module _tensioner_block(){
    difference(){
        translate([0,-width/2-tensioner_block_thickness,-height/2]) cube([length,tensioner_block_thickness,height*.45]);
        //tensioner 
        translate([0,-tensioner_block_cap_thickness,-tensioner_slot_width-x_axis_clamp_height()/2]){
            translate([0-e,-width/2,0]) tensioner();
            translate([0-e,-width/2,-belt_distance-belt_width]) tensioner();   
            translate([length,-width/2,0]) mirror([1,0,0]){
                translate([0-e,0,0]) tensioner();
                translate([0-e,0,-belt_distance-belt_width]) tensioner();
            }
        }
        screws();
        nut_h = nut_height(str("M",screws));
        //cap screws
        cap_screws();
        
        translate([length/2, -width/2-e, 0]) rotate([90,0,0]) cylinder(d=modular_x_bolt*2.6, h=tensioner_block_thickness+2e);    
//       translate([length/2, -width-e, 0]) rotate([90,0,0]) rotate(30) nutcatch_parallel(str("M",modular_x_bolt), l=width/2+2*e);
    }
}

/**
* The tensioner block cap while not strictly necessary allows easier
* printer R&D by helping keep the belts in place while changing the
* x-carriage. This makes changing the x-carriage much easier in order
* to test different configurations. This module renders the screws that
* fasten the cap to the tensioner block.
*
*/
module cap_screws(){
    t = tensioner_block_cap_thickness;
    translate([2.5*length/8, -width/2+t+e, -height/8]) screw();
    translate([5.5*length/8, -width/2+t+e, -height/8]) screw();
    translate([2.5*length/8, -width/2+t+e, -3.5*height/8]) screw();
    translate([5.5*length/8, -width/2+t+e, -3.5*height/8]) screw();

    module screw(){
        rotate([90,0,0]){
            //screw head space
            translate([0,0,-t]) {
                h=3;
                cylinder(d=6, h=h);
                translate([0,0,h]) mirror([0,0,1]) conical_space();
            }
            //screw
            cylinder(d=3.5, h=20); 
            //nut
            nut_length=4; //
            translate([0,0,tensioner_block_thickness+e+nut_length]) nutcatch_parallel("M3",l=nut_length,clk=0.5);
            conical_space();
        }
    }
}

/**
* Conical space so that the nutcatches/headspaces can be printed upside down
*
* @param d_screw float The diameter of the screw
* @param d_nut float The diameter of the screw head or nut
*
*/
module conical_space(d_screw=screws, d_nut=screws+3){
    translate([0,0,-layer*3+e]) cylinder(d=d_screw, d2=d_nut, h=layer*3);
}

/**
* The tensioner block cap while not strictly necessary allows easier
* printer R&D by helping keep the belts in place while changing the
* x-carriage. This makes changing the x-carriage much easier in order
* to test different configurations.
*/
module tensioner_block_cap(){
    translate([0,0,0]) intersection(){
        translate([0,-width/2-tensioner_block_cap_thickness,-height/2]) cube([length,tensioner_block_cap_thickness,height*.45]);
        _tensioner_block();
    }
}

/**
* Render the hulls of bronze bushings for subtraction.
* The hulls will be subtracted from the body of the 
* x-carriage in order to create recesses that hold the 
* bushings
*
* @param tolerance float How much bigger should the hulls be
* @param tolerance_h float Separate tolerance in the x-axis 
*                          helps because printers have different
*                          inacuracies along axes (especially z) 
*
*/
module bushings(tolerance=0,tolerance_h=ctol){
    two_bushings(tolerance,tolerance_h=ctol);
    mirror([0,0,1]) two_bushings(tolerance*2,tolerance_h=ctol);
    
    module two_bushings(tolerance=0,tolerance_h=ctol){
        translate([length/2,0,0]) {
            bushing(tolerance,tolerance_h=ctol);
            mirror([1,0,0]) bushing(tolerance,tolerance_h=ctol);
        }    
    }
    module bushing(tolerance=0,tolerance_h=ctol){
        translate([length/2-bushings_h-sthick,0,-x_carriage_rod_distance/2]) rotate([0,90,0]) cylinder(d=bushings_d+tolerance, h=bushings_h+tolerance_h);
    }
}

/**
* The tensioner is the part subtracted from the tensioner block
* in order to leave the space for the belt to go through and get
* squeezed by the screws. The tensioner nuts are attached to the
* end of the screws in order to push the belt without danger of
* damaging the belt.
*
* @param nut bool Whether to render the tensioner or just the nut
*                 of the screw
*
* @return  
*/
module tensioner(nut=false){
    module screw(){translate([0,-w/2,h/2]) rotate([0,90,0]) cylinder(d=tensioner_screw, h=2*(sthick+2e));}
    module tensioner_space(lrect = lrect, tolerance = 0){
        translate([sthick,-w,0]) cube([ lrect-sthick-tolerance, w-tolerance+e, h-tolerance]);
        translate([lrect-tolerance,(-w-tolerance)/2,0]) cylinder(d=w-tolerance, h=h-tolerance); 
    }
    l = 0.45*length;
    lrect = l - tensioner_thickness/2;
    w = tensioner_thickness;
    h = tensioner_slot_width;
    if (nut == false) {
        //slot
        translate([0,-w,0]) cube([ l, tensioner_slot_thickness+e, h]); 
        //belt clamp
        translate([0,-tensioner_slot_thickness/2,0]) cube([ l/2, tensioner_slot_thickness/2+e, h]);
        //screw hole (maybe add a nutrap)
        screw();
        tensioner_space();
    } else {
        difference() {
            tensioner_space(tensioner_screw*4, tolerance=0.5);           
            screw();
        }
    }
}

/**
* Renders the screws that fasten the two x-carriage pieces together
* as well as those that fasten the tensioner block on the x-carriage.
* This module does not render threads. It is just for subtraction (i.e.
* to create the holes required)
*
*/
module screws(){
    if (!onepiece) two_screws();
    if (!onepiece) translate([0,0,height/2-sthick]) two_screws();
    //extra screws for belt tensioner
    //middle pair
    translate([0,0,-height/8]) two_screws(long=true);
    if (!onepiece) translate([0,0,height/2-sthick]) two_screws();
    //bottom pair
    translate([0,0,-(height/2-sthick)]) two_screws(long=true);
    
    module two_screws(long){
        translate([1*length/6,0,0]) screw(long);
        translate([5*length/6,0,0]) screw(long);
    }
    module screw(long=false){
        translate([0,width/2-e,0]) rotate([90,0,0]) {
            screw_length = long?width+tensioner_block_thickness:width;
            cylinder(d=screwst, h=screw_length);
            //head space
            translate([0,0,screw_length-screws]){
                cylinder(d=screws+3, h=screws);
                //conical space so that the nutcatches/headspaces can be printed upside-down
                conical_space();
            }
            //nuts
            nut_h = nut_height(str("M",screws));
            translate([0,0,nut_h-2e]) {
                nutcatch_parallel(str("M",screws), l=nut_h, clk=0.5);
                translate([0,0,-e]) cylinder(d=screws*2, d2=screws, h=3*layer);
            }
        }
    }
}

/**
* The original design had a pcb on the x-carriage carrying all the possible
* electrical signals for single extruder, dual extruder, proximity sensor,
* spindle, fans etc. This has not been realized and this is not final. TBD
*
*/
module pcb_slot(){
    p = x_carriage_pcb_thickness;
    w = 2;
    translate([0,width/2,modular_x_bolt*1.5]) {
        difference(){
            cube([length,p+2*w,x_carriage_pcb_height+6]);
            translate([-1,w,3]) cube([length+2,p,x_carriage_pcb_height]);
            translate([-1,p,w+p]) cube([length+2,p+w,x_carriage_pcb_height-w-p]);
        }
    }
}

/**
* When mounting dual e3d extruders the extruders protrude from the x-carriage
* and hit the y-carriage before the x-carriage hits the endstop. This was an
* attempt to extend the x-carriage enough in order for it to hit the endstop
* first. However this is not the right solution (breaks modularity) so it is
* deprecated. It is left here until I develop a solution that extends the 
* extruder mount itself.
*
*/
module endstop_extension(){
    w=20;
    t=5;
    h=10;
    
    y=onepiece?-_wwide/2:-width/2;
    translate([length,y,_height/2-h]) cube([15,t,h]);
}

/**
* A cylinder used for rounding corners
*
*/
module roundness(){
   translate([0,0,_height/2]) rotate([0,90,0]) cylinder(d=width, h=length);
}

/**
* Return the size of the x-carriage
*
* @return array3 x,y,z dimensions of the x-carriage
*/
function x_carriage_size() = [length,width,height];

/**
* Return the size of the tensioner block
*
* @return array3 x,y,z dimensions of the tensioner block
*/
function tensioner_block_size() = [length, tensioner_block_thickness, height/2];

/**
* Return the size of the tensioner block cap
*
* @return array3 x,y,z dimensions of the tensioner block cap
*/
function tensioner_block_cap_size() = [length, tensioner_block_cap_thickness, height/2];
