/**
* The part that mounts the y-axis rails to the frame of the printer
*
*/
use <../lib/ISOThreadUM2.scad>
use <../lib/nutsnbolts/cyl_head_bolt.scad>
include <../general-values.scad>
include <../config/calculated-values-2.scad>  
use <../lib/utl.NEMA.scad>
use <../parts/x-axis-clamp.scad>
use <../assemblies/extruder.scad>
use <../lib/mechanical.scad>
use <x-carriage.scad>
 
/* Print or Preview */

print_all_y_axis_clamps();
// y_axis_clamp_internal(idler=true, endstop=true);

/* Constants */

// The "with endstop" version cannot be printed without support if 
// `flip_secondary_axis` is false. Either enable supports on your 
// slicer, or set one of the two options below to true.
support = false; //set this to true to make this part easier to print
two_parts = false; //another way to make easier to print
bolt_min_dist = 22;
center = false;
slab_thickness = 10;

/* Calculated Values */

rod = y_rod_d;
bolt = frame_bolts;
height = max(y_clamps_angle_height,y_clamps_slab_height/2);
thickness = minimum_thickness;
support2 = two_parts?false:support;
angle_height = y_clamps_angle_height;
slab_height = y_clamps_slab_height;
nema = nema17_size(nema_length)[0];
idler_shelf_t = belt_distance-idler_height+belt_width;
lower_x_carriage = x_axis_clamp_size()[2]/2+belt_width+belt_distance;
angle_thickness=profile_width/3;

rodt = rod + mtol;
boltt = bolt+mtol;
bolt_Z = angle_height+profile_width/2; //bolt position on slab
idler_hole_offset = idler_flange_diameter/2+2*mtol; //needs tolerance so that flange doesn't touch the angled part
idler_offset = [-rod/2-slab_thickness-idler_hole_offset,
                -idler_shelf_t-x_axis_clamp_size()[2]/2-idler_height-idler_flange_thickness,
                profile_width/4];
//minimum_thickness/2 is the thickness of motor mount plates 
slab_width=max(
    bolt*4,
    bolt_min_dist*1.75,
    -idler_offset[1]+belt_width*1.4+idler_shelf_t*2,//one belt is included in idler offset so we need it's 0.2 extra
    x_axis_clamp_height()/2+y_rod_d/2+idler_height+idler_shelf_t+idler_flange_thickness
);
echo(idler_offset);
ifcenter = center ? slab_width/2 : 0;

/* Checks */

// check if the y-carriage is big enough to hit the
// endstop if the enstop is flush with the clamp
echo("x_axis_clamp_size[2]/2", x_axis_clamp_size()[2]/2);
echo("x_axis_clamp_size()[2]/2+belt_width+belt_distance", x_axis_clamp_size()[2]/2+belt_width+belt_distance); //lower_x_carriage
echo("endstop_width*0.4+rod/2+thickness*1.5",endstop_width*0.4+rod/2+thickness*1.5);
fac = can_flip_endstop?0.4:0.8;
endstop_flush0 = lower_x_carriage>endstop_width*0.4+rod/2+thickness*1.5;
endstop_flush1 = slab_width-rod-2.5*thickness>endstop_width*0.8;
endstop_flush = endstop_flush0 && endstop_flush1;

//check if the y-carriage is big enough to hit the endstop anyway
endstop_check=x_axis_clamp_size()[2]/2>endstop_height*0.8+rod/2;

if (!endstop_check) echo ("ERROR: endstop does not fit");

fit_endstop=endstop_flush?0:endstop_height;
y_home = flip_secondary_axis?
         y_axis_clamp_y_min()+extruder_size()[1]+extruder_drive_thickness:
         slab_height-profile_width+acrylic_position+fit_endstop;
y_total = flip_secondary_axis?
    max(angle_height, endstop_height):
    max(y_home-angle_height,0);

/* Modules */

/**
* Render the four y-axis clamps for printing
*
*/
module print_all_y_axis_clamps(){
    extension = nema17_size()[0] + profile_width - slab_height;
    mirror([1,0,0]) print();
    translate([0,y_axis_clamp_size()[0]*1.5,extension]) print(idler=false, endstop=true);
    translate([y_axis_clamp_size()[0]*1.5,0,0]) print(idler=true);
    translate([y_axis_clamp_size()[0]*1,y_axis_clamp_size()[0]*1.5,0]) mirror([1,0,0]) print(idler=true);
}

/**
* Render the y-axis clamp in a way that is most convenient for
* 3D printing
*
* @param idler   bool Whether to print the shelf where the 
*                     two timing belt idlers will be mounted
* @param endstop bool Whether to accommodate an endstop switch.
*                     These two values should not both be true
*                     as the belt will collide with the endstop
*/
module print(idler=false, endstop=false){
    difference(){
        y_axis_clamp_internal(idler, endstop, print=true);
        if (two_parts&&!flip_secondary_axis)  translate([-rod/2-slab_thickness,-slab_width+thickness,0]){
            print_endstop_holder(prongs=true);
        }
    }
    if (two_parts&&!flip_secondary_axis&&endstop) translate([-60,-slab_width+rod,0]) rotate([0,-90,0]) print_endstop_holder();
}

/**
* Render the y-axis clamp laid out for inclusion in the general assembly
*
* @param idler   bool Whether to print the shelf where the 
*                     two timing belt idlers will be mounted
* @param endstop bool Whether to accommodate an endstop switch.
*                     These two values should not both be true
*                     as the belt will collide with the endstop
*/
module y_axis_clamp(idler=false, endstop=false){
    translate([0,bolt_Z,0]) rotate([90,0,0]) y_axis_clamp_internal(idler, endstop);
}

/**
* Render the y-axis clamp laid out for development
*
* @param idler   bool Whether to print the shelf where the 
*                     two timing belt idlers will be mounted
* @param endstop bool Whether to accommodate an endstop switch.
*                     These two values should not both be true
*                     as the belt will collide with the endstop
*/
module y_axis_clamp_internal(idler=false, endstop=false, print=false){
    rod_clamp(endstop);
    if (idler) {
        _idler(print);
        step = idler_height+idler_shelf_t+mtol;
        translate([0,step,0]) _idler(true);
        translate([0,-step,0]) _idler(true);
        difference(){
            slab(endstop = endstop, print=print);
            belt_slot();
        }
    } else {
        // on the side of the upper motor we don't have enough height for the full width
        // the full width is needed on the side with the idlers to accommodate the two
        // slots sans overhangs
        slab(endstop = endstop, print=print, slab_width = lower_x_carriage+teeth_offset-minimum_thickness/2);
    }
}

/**
* Locally positioned representation of the idler shelf.
* This is a protrusion from the y-axis clamp on which two 
* toothed timing belt aluminium idlers are mounted.
*
*/
module _idler(print=false){
    module hole(){
        translate([0,0,-e]) cylinder(minimum_thickness+e,d=idler_inner_diameter, $fn=16);
    }
    module transform(){
        rotate([90,0,0]) translate([0,profile_width/4,-idler_shelf_t]) children();
    }
    translate(idler_offset-[angle_thickness/2,0,profile_width/4]) {
        difference(){
            cube([idler_hole_offset+angle_thickness/2,idler_shelf_t,profile_width/2]);
            transform() hole();
        }
        vitamin(!print) transform() {
            translate([0,0,idler_shelf_t]) cylinder(h=idler_height,d=idler_diameter);
            translate([0,0,-idler_height]) cylinder(h=idler_height,d=idler_diameter);
        }
        transform() {
            difference () {
                cylinder(idler_shelf_t,profile_width/4,profile_width/4);
                hole();
            }
        }
    }
    
}

/**
* The belt passes through the part in it's way to the other side. This is
* the positive representation of the slot, created to be subtracted from
* the part to create the actual belt passage.
*
*/
module belt_slot(){
    x = -slab_thickness-rod/2-profile_width; //where profile_width is just a sufficiently big number
    y = idler_offset[1]+idler_shelf_t;
    z = idler_offset[2] + idler_diameter/2 - belt_thickness/2;
    
    translate([x,y,z]) cube([profile_width*2, belt_width*1.2, belt_thickness*2]);
    translate([x,y-idler_shelf_t-belt_width*1.2,z]) cube([profile_width*2, belt_width*1.2, belt_thickness*2]);
}

/**
* The part of the y-axis clamp that mounts to the aluminium frame
*
* @param angle          bool      Whether to reinforce the mounting slab with an angled part
*                                 that hugs the aluminium profile 
* @param slab_height    float     The Z dimension of the mounting slab
* @param slab_width     float     The Y dimension of the mounting slab
* @param endstop        bool      Whether to accomodate for an endstop switch
*
*/
module slab(angle=true, slab_height=slab_height, slab_width=slab_width, endstop=false, print=false){
    extension = endstop ? nema17_size()[0] + profile_width - slab_height : 0 ;

    //angle
    if (angle) translate([-slab_thickness-angle_thickness-rod/2,-slab_width+thickness,0]) hull(){
        translate([angle_thickness,0,0]) cube([0.01,slab_width,thickness]);
        translate([0,0,angle_height]) cube([angle_thickness,slab_width,0.01]);
    }
    
    difference(){
        translate([-slab_thickness-rod/2,center?-slab_width/2:-slab_width+thickness,0]) {
            difference(){
                //slab
                translate([0,0,-extension]) cube([slab_thickness, slab_width, slab_height+extension]);
                //bolts
                translate([0,0,bolt_Z]){
                    translate([-e,slab_width/4,0]) rotate([0,90,0]) cylinder(d=boltt, h=height+2e);
                    translate([-e,3*slab_width/4,0]) rotate([0,90,0]) cylinder(d=boltt, h=height+2e);
                }
            }
            //if (print==false) translate([-7,0,0]) bolts();
            if (endstop) endstop_holder(print);
        }
        translate([0,0,-e-y_total-extension]) cylinder(d=rodt, h=slab_height+2e+y_total+extension);
    }
}

/**
* In some cases the clamp needs an extension in order to fit the endstop. 
* This is the part on which the endstop switch mounts in case it is required.
* This is not always used. Depending on the endstop model and `flip_secondary_axis
* the endstop switch might fit on the part itself.
*
*/
module endstop_holder(print=false){
    clearance=flip_secondary_axis?0:rod;
    translate([slab_thickness,(slab_width-endstop_width)-clearance,-y_total]) rotate(90) 
        cube([slab_width,slab_thickness+2e,y_total]);
    more = y_total > 0 ? 0 : 2*thickness;
    if (!flip_secondary_axis) translate([slab_thickness,slab_width-clearance-more,-y_total+endstop_height]) rotate([0,90,0]) rotate([0,0,-90]){
        endstop(endstop_model, show=!print);
    }
}

/**
* When a special extension for mounting the endstop
* is required there is a case where this extension
* must be printed separately and glued to the y-axis clamp
*
* @param prongs bool Set to true if you want small protrusions that 
*                    snap into the makerbot endstop holes 
*/
module print_endstop_holder(prongs){
    difference(){
        endstop_holder(print=true);
        if (!prongs) holes(4);
    }
    if (prongs) holes();
    module holes(diameter=3){
        translate([slab_thickness/2,endstop_width/4,-y_total-e]) {
            cylinder(d=diameter, h=nema-angle_height+e+angle_height/2);
            translate([0,endstop_width/2,0]) cylinder(d=diameter, h=nema-angle_height+e+angle_height/2);
        }
    }
}

/**
* The part of the y-axis clamp that actually clamps the smooth rod
*
* @param endstop bool Whether this particular clamp accomodates
*                     an endstop switch
*
*/
module rod_clamp(endstop=false){
        endstop_modifier=endstop&&(flip_secondary_axis||support2)?y_total:0;
        extension = endstop ? nema17_size()[0] + profile_width - slab_height : 0 ;
        difference(){
        //rod holder
        translate([0,0,-endstop_modifier]) union() {
            //ears
            translate([rodt/2,-3*thickness/2,-extension]) cube([thickness*2,thickness,height+endstop_modifier+extension]);
            translate([rodt/2,1*thickness/2,-extension]) cube([thickness*2,thickness,height+endstop_modifier+extension]);
            //circle
            difference(){
                translate([0,0,-extension]) cylinder(d=rodt+thickness*2, h=height+endstop_modifier+extension );
                translate([0,0,-e-extension]) cylinder(d=rodt, h=height+endstop_modifier+2e+extension);
                translate([0,-thickness/2,-e-extension]) cube([thickness*4,thickness,height+endstop_modifier+2e+extension]);
            }
            //endstop_holder
            if(endstop&&flip_secondary_axis) translate([-endstop_width/2,rodt/2,0]) {
                difference(){
                    cube([endstop_width,thickness,endstop_height]);
                }
                
                translate([0,endstop_thickness-1,endstop_height]) rotate([-90,0,0]){
                    %import("lib/MakerBot_Endstop.stl"); //TODO: this should be endstop()
                    endstop_holes();
                }
            }
            
            //stiffness triangle
            translate([-thickness/2,-rod/2-thickness,0])  cylinder(r=thickness, h=height+endstop_modifier, $fn=3);
        }
        //screw
        h=(thickness*2+rodt)*2;
        translate([rodt/2+thickness,h/2,height/2]) rotate([90,0,0]) cylinder(d=3+mtol, h=h);
        
        //nut & head traps
        translate([rodt/2+thickness,3*thickness/2+e,height/2]) rotate([90,0,0]) cylinder(d=6, h=3);
        translate([rodt/2+thickness,-3*thickness/2-e,height/2]) rotate([90,0,0]) nutcatch_parallel("M3", l=3);
        // what is this?
        // if (!flip_secondary_axis) translate([-rodt/2,-slab_width+rodt/2,-y_total]) cube([endstop_thickness*2,endstop_width+10,endstop_height]);
    }
}

/**
* Shape of mounting bolts without threads. To be subtracted
* from the y-axis clamp in order to create the mounting holes
*
*/
module bolts(){
    translate([25,slab_width/4,slab_height/2]) rotate([0,90,0]) rotate([0,180,0]) hex_bolt(dia=boltt, hi=height);
    translate([25,3*slab_width/4,slab_height/2]) rotate([30,0,0]) rotate([0,90,0]) rotate([0,180,0]) hex_bolt(dia=boltt, hi=height);
}

/**
* Returns the thickness of the y-axis clamp. 
* (The thickness of the clamping part) 
*
* @return float The thickness of the curved 
*               part that squeezes the rod
*/
function y_axis_clamp_thickness() = thickness;

/**
* Returns the thickness of the mounting slab
* of the y-axis clamp (the part that fastens
* to the aluminium frame)
*
* @return float The thickness of the mounting 
*               slab of the y-axis clamp
*/
function y_axis_slab_thickness() = slab_thickness;

/**
* Returns the width of the mounting slab
* of the y-axis clamp (the part that fastens
* to the aluminium frame)
*
* @return float The width of the mounting 
*               slab of the y-axis clamp
*/
function y_axis_clamp_slab_width() = slab_width;

/**
* Returns the total size of the y-axis clamp
*
* @return array3 x,y,z dimensions of the clamp
*/
function y_axis_clamp_size() = [thickness*2+rodt+slab_thickness*2.5,slab_height,slab_width+minimum_thickness];

/**
* The y-axis clamp is positioned so its local
* origin point is in the middle of the rail.
* These are the coordinates of the beginning
* of the clamp based on that origin
*
* @return array3 How much you have to move
*                away from the clamps position
*                in order to stay clear
*/
function y_axis_clamp_offset() = [rod/2+slab_thickness,slab_height-profile_width+acrylic_position,0];

/**
* Deprecated: use `y_axis_clamp_offset()[0]` instead 
*
* @return array3 How much you have to move
*                away from the clamps position
*                in order to stay clear
*/
function y_axis_clamp_x_offset() = rod/2+slab_thickness;

/**
* Returns the length of the part of the y-axis clamp
* that protrudes outside the "inner cube", in other
* words the part that hugs the frame.
*
* @return float The height of the frame-hugging angle
*/
function y_axis_clamp_y_min() = angle_height;

/**
* Returns the xy coordinates of the idler relative
* to the intersection of center of the rod with the 
* innermost XZ plane of the clamp
*
* @return array3 x,y,z coordinates of the idler mounting
*                      hole
*/
function y_axis_clamp_idler_offset() = idler_offset;