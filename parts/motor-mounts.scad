/**
* This part mounts the x, y and z motors on the aluminium frame
*
*/
include <../general-values.scad>
include <../config/calculated-values-2.scad> 
use <angle-fastener.scad>
use <../lib/utl.NEMA.scad>
use <y-axis-clamp.scad>

/* Print or Preview */

print();
//preview();

/* Calculated Values */

plate_thickness=minimum_thickness;
taper=tapered_angle_fastener_bolts?4.5:0;
hole=tapered_angle_fastener_bolts?11:angle_fastener_bolts+mtol*2;
slab_height = y_clamps_slab_height;

mount_brick_width = profile_width-acrylic_position;

mx=-nema17_size(nema_length)[0]/2;
my=nema17_size(nema_length)[0]/2;
mz=nema17_size(nema_length)[2];

/* Modules */

/**
* Render the xy motor mount along with the motor for
* previewing purposes
*
*/
module preview(){
    XY_motor_mount();
    nema17(nema_length);
    translate([nema17_size()[0]*2,0,-nema_length]){
        z_motor_mount_top(motor=true);
        z_motor_mount_bottom();
    }
}

/**
* Render the xy motor mount in a way best suited for
* inclusion in the general assembly
*
*/
module XY_motor_mount(){
    translate([-mx,-my,-mz]){
        XY_motor_mount_internal(top=true);
        XY_motor_mount_internal(top=false);
    }
}


/**
* Render the xy motor mount in a way best suited for
* 3D printing without supports
*
*/
module print(){
    print_one(left=true);
    translate([nema17_size()[0]*2.5,0,0]) print_one(left=false);
    // translate([-nema17_size()[0]/2,-nema17_size()[0],0]) print_z_motor_mount();
}

/**
* Render the left xy motor mount in a way best suited
* for 3D printing without supports
*
*/
module print_left(){
    translate([0,-10,plate_thickness]) rotate([0,0,0]) XY_motor_mount_internal();
    translate([0,0,plate_thickness]) rotate([0,180,0]) translate([0,nema17_size(nema_length)[0]+20,-nema_length]) XY_motor_mount_internal(true);
}

/**
* Render either of the motor mounts
*
* @param left bool Whether to render the left or the
*                  right mounts 
*/
module print_one(left=false){
    mi = left?[0,0,0]:[1,0,0];
    mirror(mi) print_left();
}

/**
* Render the xy motor mount without transformations i.e.
* best suited for develompent
*
*/
module XY_motor_mount_internal(top=false){
    txx = nema17_size(nema_length)[0]/2;
    tzz = top?(nema_length)/2:-plate_thickness;
    tz_hole = top?3*nema_length/4:nema_length/4;

    difference(){
        translate([0,0,tzz]) cube([mount_brick_width,y_axis_slab_thickness(),(nema_length)/2+plate_thickness]);
        translate([profile_width/2, 0-e, tz_hole]) rotate([-90,0,0]) cylinder(d=frame_bolts+mtol, h=15+e);
        if (top) pin(0.1);
    }
    slabExtended = nema17_size(nema_length)[0]>profile_width+y_axis_slab_thickness()?0:nema17_size(nema_length)[0];
    rest_thickness = (nema17_size(nema_length)[0] - profile_width - y_axis_slab_thickness());
    difference(){ //second plate (reinforcement)
        translate([-slabExtended,profile_width+y_axis_slab_thickness(),tzz]) cube([mount_brick_width+slabExtended,rest_thickness,nema_length/2+plate_thickness]);
        translate([profile_width/2, profile_width+y_axis_slab_thickness()-e, tz_hole]) rotate([-90,0,0]) cylinder(d=frame_bolts+mtol, h=rest_thickness+2e);
        if (top) pin(0.1);
    }
    if (!top) pin();

    //plates
    translate([mx,my,0]) plate(top=top);
}

/**
* Plate above or below the motor with mounting holes
*
* @param top       bool   Whether to render the plate for the top
*                         of the motor or the one below
* @param zeroz     bool   One of the two motors is slightly above
*                         the other so the belts are parallel and
*                         one on top of the other. When zeroz is 
*                         true this instance is moved upwards 
* @param thickness float  The thickness of the plate
*
*/
module plate(top=true, zeroz=false, thickness=plate_thickness){
    difference(){
        XY_motor_mount_plate(top=top, zeroz=zeroz, thickness=thickness);
        //holes
        translate([0,0,nema_length*3.5]) nema_holes( nema17_holes()[0]+mtol, nema17_holes()[1]+mtol,nema_length*2);
    }
}

/**
* This is a pin that connects the top and the bottom parts.
* It is not strictly necessary and it is prone to breakage but
* if it stays in place it makes for a sturdier result.
*
* @param tolerance float The pin protrudes from the bottom part 
*                        and fits into a hole in the top part. 
*                        The tolerance makes for an easier fit. 
*/
module pin(tolerance=0){
    d=y_axis_slab_thickness()/2+tolerance;
    translate([3*profile_width/4-d, d, nema_length/2]) cylinder(d=d, h=10);
}

/**
* Plate above or below the motor, without holes
*
* @param top       bool   Whether to render the plate for the top
*                         of the motor or the one below
* @param zeroz     bool   One of the two motors is slightly above
*                         the other so the belts are parallel and
*                         one on top of the other. When zeroz is 
*                         true this instance is moved upwards 
* @param thickness float  The thickness of the plate
*
*/
module XY_motor_mount_plate(top=true, zeroz=false, thickness=minimum_thickness/2){
    x = nema17_size(nema_length)[0];
    y = max(nema17_size(nema_length)[0],profile_width+y_axis_slab_thickness()*2);
    h = thickness;
    tz = zeroz?0:mz+h/2;
    // echo ("tz",tz);
    if (top)
        translate([0,0,tz]) difference(){    
            translate([mount_brick_width/2,(y-x)/2,0]) cube([x+mount_brick_width,y,h], center=true);
            translate([0,0,-5]) cylinder(d=motor_shaft_hole, h=20);
        }
    else
        translate([mount_brick_width/2,(y-x)/2,-h/2]) cube([x+mount_brick_width,y,h], center=true);
}

/**
* Render the top part of the motor mount for the z motor(s)
*
* @param motor bool Whether to show the motor itself or not
*
*/
module z_motor_mount_top(motor = false){
    half_nema = nema17_size()[0]/2;
    if (motor) translate([0,0,nema_length]) nema17(nema_length);
    plate();
    translate([-half_nema,-half_nema-minimum_thickness,profile_width]) cube([nema17_size()[0],minimum_thickness,nema_length-profile_width+plate_thickness]);
    translate([-half_nema,-half_nema-profile_width,profile_width]) difference(){    
        cube([nema17_size()[0],profile_width,plate_thickness]);
        translate([half_nema/2, profile_width/2, -e]) cylinder(d=frame_bolts, h=plate_thickness+2e);
        translate([1.5*half_nema, profile_width/2, -e]) cylinder(d=frame_bolts, h=plate_thickness+2e);
    }
}

/**
* Render the bottom part of the motor mount for the z motor(s)
*
*/
module z_motor_mount_bottom(){
    half_nema = nema17_size()[0]/2;
    plate(top=false);
    translate([-half_nema,-half_nema-profile_width,-plate_thickness]) difference(){    
        cube([nema17_size()[0],profile_width,plate_thickness]);
        translate([half_nema/2, profile_width/2, -e]) cylinder(d=frame_bolts, h=plate_thickness+2e);
        translate([1.5*half_nema, profile_width/2, -e]) cylinder(d=frame_bolts, h=plate_thickness+2e);
    }
}

module print_z_motor_mount(){
    translate([0,0,nema17_size()[0]/2]) rotate([0,90,0]) z_motor_mount_top();
    translate([nema17_size()[0]*2,0,plate_thickness]) z_motor_mount_bottom();
}

/**
* Return the thickness of the mounting slab (the part that fastens to the
* aluminium profile) of the xy motor mount
*
* @return float The thickness of the slab
*/
function XY_motor_mount_slab_thickness() = y_axis_slab_thickness();