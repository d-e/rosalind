/**
* The parts that clamp the bed frame to the z-axis smooth rods
*
*/
include <../general-values.scad>
use <../lib/mechanical.scad>
use <z-axis-clamp.scad>
use <../lib/utl.NEMA.scad>
use <../lib/nutsnbolts/cyl_head_bolt.scad>
use <../assemblies/bed.scad>
use <../vitamins/aluminium-profile.scad>

/* Print or preview */

print_all_bed_clamps();
// bed_clamp(endstop_adjuster=true, bearing=true);
//rotate([90,0,0]) thickener();

/* Constants */

/**
 * Structural thickness: a thickness that will be able to 
 * have some structural integrity with your material and 
 * print settings
 * 
 */
sthick = 6;

/* Some parts should be thicker for even better support
 * How much thicker?
 *
 */
thicker = 3;

/* Calculations */

// for bigger structures add more stiffness by clamping at
// the bottom of the bed too.
rigid = external>400?true:false;

// The x-dimension of the clamp
_length = bearing_size(z_linear_bearing)[1]/2+sthick+thicker;
minimum_length = (external - profile_width*2 - z_axis_clamp_x_mountpoint_offset()*2 - bed_size()[1])/2;
// don't go below minimum_length
length = _length < minimum_length ? minimum_length : _length;

// echo(str("length: ",length));
// echo(str("Padding: ",length-_length));

// y and z dimensions of the clamp
height = bed_profile;
width = bed_profile*2;

// just get the value from `general-values.scad`
bolts = frame_bolts;

/* Modules */

/**
* Lays out all the needed bed clamps for this printer
* for printing
*
*/
module print_all_bed_clamps(){
    print(endstop_adjuster=true);
    translate([width*1.5, 0, 0]) mirror() print();
    if (rigid) translate([0, width*2, 0]) {
        print();
        translate([width*1.5, 0, 0]) mirror() print();
    }
}

/**
* Render the bed clamp laid out for printing
*
* @param endstop_adjuster One of the bed clamps should have a
*                         hole for the screw that hits the Z endstop
*
*/
module print(endstop_adjuster = false){
    if (rigid) back(extended=false);
    else translate([0,0,length]) rotate([90,0,0]) back();
    //translate([0,10,rigid?0:length]) rotate([0,180,0]) front(endstop_adjuster);
}

/**
* Render the rounded part of the bed clamp. This is just cosmetic
* It could well be a cube.
*
* @param height float The Z dimension of the clamp
*
*/
module section(height){
    rounded_corner(_length, height);
    mirror([1,0,0]) rounded_corner(_length,height);
    translate([-width/4,0,0]) cube([width/2,_length,height]);
    translate([-width/2,0,0]) cube([width,_length/2,height]);
}

/**
* Render the outer half of the clamp, screw-holes and all.
*
* @param endstop_adjuster boolean One of the bed clamps should have a
*                                 hole for the screw that hits the Z endstop
*
*/
module front(endstop_adjuster = false){
    half_nema = nema17_size(nema_length)[0]/2;
    height=rigid?height*2:height; //rigid clamp has double the height
    // cut out the bearing and screw holes
    translate([0,0,rigid?-height/2:0]) difference(){
        section(height);
        hole();
        screws();
        translate([-width/2,-length,0]) cube([width,length,height]);
    }
    // endstop adjuster
    if (endstop_adjuster){
        eaw = width/3;
        eath = minimum_thickness;
        translate([-eaw/2,_length,rigid?height/2-eath:height-eath]) difference(){
            // this needs to be as thick as the enstop so 
            // that the screw will hit it right in the middle
            cube([eaw,endstop_thickness,eath]);
            translate([eaw/2,endstop_thickness/2,-1]) {
                cylinder(d=3, h=20);
                // translate([0,0,3]) nutcatch_parallel("M3", l=5);
            }
        }
    }
}

/**
* Clamps for mounting the bed frame to the Z smooth rods
*
* @param endstop_adjuster bool One of the bed clamps should have a
*                              hole for the screw that hits the Z endstop
* @param bearing          bool Whether to display the linear bearing
*
*/
module bed_clamp(endstop_adjuster = false, bearing = false){
    front(endstop_adjuster);
    back();
    if(bearing) bearing(name=z_linear_bearing);
}

/**
* Size of the bed clamps
*
* @return array3 x, y and z dimensions of the bed clamp
*/
function bed_clamps_size() = [width,length+_length,height];

/**
* Size of the outer (curved) part of the bed clamps
*
* @return array3 x, y and z dimensions 
*/
function bed_clamps_size_outer() = [width, _length, height];

/**
* Size of the inner part of the bed clamps (the one that)
* is attached to the bed
*
* @return array3 x, y and z dimensions 
*/
function bed_clamps_size_back() = [width, length, height];

/**
* A 2D rounded corner :-P
* you can unionize it (is that a word?) to other solids to make
* them have rounded corners
*
* @param size   float  The size of the cross-section of the corner
* @param height float  The height of extrusion
*
*/
module rounded_corner(size = length, h = height){
    hull() {
        translate([width/4,size-width/4,0]) cylinder(d=width/2, h=h);
        translate([-width/2,0,0]) cube([width/4, width/4/2, h]);
    }
}

/**
* The part of the bed clamp that attaches to the bed frame
*
* @param extended bool Some times the clamp needs to cover additional 
*                      Distance to fill the space between the rods and 
*                      the bed frame (when the bed frame size is fixed)
*
*/
module back(extended = true){
    half_nema = nema17_size(nema_length)[0]/2;
    height=rigid?height*2:height;
    translate([0,0,rigid?-height/2:0]) difference(){
        union(){
            translate([-width/2,-length,0]) cube([width,length,height]);
            if (rigid){
                //extra part
                translate([-width/2,-length-bed_profile,0]) cube([width,bed_profile,height/2]);
            }
        }
        hole();
        screws();
    }
    if (extended) {
        minkowski(){
        difference(){
            intersection(){
                translate([profile_width,-length,0]) cube([profile_width/3,length,height]);
                translate([bed_profile/2,-2*length,bed_profile/2]) rotate([0,45,0]) cube([bed_profile/sqrt(2), length*2, bed_profile/sqrt(2)]);
            }
            translate([width/2+bed_profile/2,-length/2,bed_profile/2]) {
                minkowski(){
                    rotate([90,0,0]) bed_profile(length+1+e);
                    rotate([90,0,0]) cylinder(d1=0, d2=0.5 ,h=length/3);
                }
            }
        }
        rotate([-90,0,0]) cylinder(d=0.5, h=0.01);
        }
    }
}

/**
* Holes for all the screws (and their heads) required for the
* bed clamps. For subtraction from the bed clamps.
*
*/
module screws(){
    short_screw_l = ceil((_length-bolts+length)/5)*5+1;
    l = rigid?short_screw_l:100;
    mirror([1,0,0]) screw(l);
    screw(l);
    if (rigid) translate([0,0,height]){
        //second pair of screws, with nutcatch
        mirror([1,0,0]) screw();
        screw();
        nutcatch();
        mirror([1,0,0]) nutcatch();
        
        //vertical screws
        yoffset=-length;
        translate([0,yoffset,0]) rotate([90,0,0]) screw();
        mirror([1,0,0]) translate([0,yoffset,0]) rotate([90,0,0]) screw();
    }
}

/**
* One (space for) the bed clamp screw
*
* @param screw_length float The length of the screws. Doesn't really matter 
*                           Here as it is just used for subtraction
*
*/
module screw(screw_length = 100){
    translate([width/3,_length+e,height/2]) rotate([90,0,0]) {
        translate([0,0,bolts]) cylinder(d=bolts+mtol, h=screw_length+2e, $fn=32);
        cylinder(d=2*bolts+mtol, h=bolts+e, $fn=32);
    }   
}

/**
* Nutcatch correctly positioned for tightening the extra
* screws of the rigid (corner) bed clamp. Only for bigger printers
*
*/
module nutcatch(){
    translate([width/3,-length,-height/2]) rotate([90,0,0]) nutcatch_sidecut(str("M",bolts), l=width/3, clh=0.2, clsl=0.15);
}

/**
* Hole in the pillow block (clamp) that houses the Z-axis bearing
*
* @param tolerance float How much bigger than the outer diameter of
*                        the bearing must the hole be. Most printers 
*                        print slightly thicker parts so this defaults
*                        to 1 
*
*/
module hole(tolerance = btol){
    $fn=64;
    if (rigid)
        translate([0,0,height]) bearing(name=z_linear_bearing, hull=true, tolerance = tolerance);
    translate([0,0,-e]) bearing(name=z_linear_bearing, hull=true, tolerance = tolerance);
}

/**
* A part that thickens the bed clamp on the (local) y axis 
*
* @param amount float The amount to thicken the clamp
*
*/
module thickener(amount=length-_length){
    difference(){
        translate([-width/2,-length,0]) cube([width,amount,height]);
        translate([0,0,-height]) screws();
    }
}

