/**
* A very awesome faceplate that proudly displays "Rosalind".
* The most important part of the printer :-P
*
* Export to amf to preserve the dual colors or set your slicer
* to print in a different color everything above 2mm
*
* (or just color it yourself :-P)
*
*/

faceplate();

/**
* Print a nice faceplate for the front of the printer
* Displays the name "rosalind" and the version number
*
*/
module faceplate(){
    // The plate
    cube([faceplate_size()[0], faceplate_size()[2], faceplate_size()[1]]);
    // The text
    color("white") translate([10,10,2]) {
        linear_extrude(height = 2) text("rosalind",font = "OpenGost Type A TT:style=Regular", size=14, center=true);
        //The version
        translate([50,0,0]) linear_extrude(height = 2)  text("v1.0",font = "OpenGost Type A TT:style=Regular", size=8, center=true);
    }
}


/**
* Returns the size of the faceplate
*
* @return array3 XYZ dimensions of the faceplate
*/
function faceplate_size() = [100, 2, 40];