/**
* Plastic feet for the printer to stand on. Required unless
* you use countersunk bolts to fasten electronics on the base.
*
*/
include <../general-values.scad>

$fn=48;

/* Print and preview */

print();
// foot();

module print(){
    rotate([180,0,0]) foot();
    translate([profile_width*1.5,0,0]) rotate([180,0,0]) foot();
    translate([0,profile_width*1.5,0]) rotate([180,0,0]) foot();
    translate([profile_width*1.5,profile_width*1.5,0]) rotate([180,0,0]) foot();
}

/**
* Plastic feet for the underside of the printer. Include recess for the frame
* bolt heads
*
*/
module foot(){
    height = profile_width/3;
    translate([profile_width/2,profile_width/2,0]) rotate([180,0,0]) difference(){
        cylinder(d=profile_width, h=height); 
        translate([0,0,height-frame_bolts]) cylinder(d=profile_width*.7, h=height); 
        translate([0,0,-e]) cylinder(d=frame_bolts+1.5, h=10+2e); 
    }
}