/**
* This file is derived from schlotzz's direct-drive extruder so it's licensed CC-BY-SA 3.0
* and not GPL2 as is the rest of Rosalind.
*
* It provides a modified extruder drive mount for mounting the geared wade's extruder 
* with mounting holes that fit the frame exactly.
* there is also an optional filler block that makes sure the motors don't protrude
* out of the acrylic enclosure
*
* The rosalind has not been tested with this extruder drive
*
*/
include <../general-values.scad>
use <../assemblies/bed.scad>
use <z-axis-clamp.scad>

use <motor-mounts.scad>
use <../lib/utl.NEMA.scad>

plate_thickness = 5;

xy = nema17_size(nema_length)[0];
h = 30;

translate([xy/2,0,plate_thickness/2]) plate(top=true, zeroz=true, thickness=plate_thickness);
//translate([xy,0,h/2]) cube([xy,xy,h],center=true);
translate() frame_mount();

/**
* The extruder drive part that attaches to the motor including 
* a mounting block for attaching to the printer main frame
*
*/
module frame_mount()
{
    epsilon = 0.01;
    filler = (extruder_nema_length-(profile_width-acrylic_position));
    nema17_width = nema17_size(nema_length)[0];
    nema17_hole_offsets = [
        [-15.5, -15.5, 1.5],
        [-15.5,  15.5, 1.5],
        [ 15.5, -15.5, 1.5],
        [ 15.5,  15.5, 1.5 + plate_thickness]
    ];
    base_width = 15;
    // increase this if your slicer or printer make holes too tight
    extra_radius = 0.1;
    
    extruder_drive_mount_plate_length = 42;

    // major diameter of metric 3mm thread
    m3_major = 2.85;
    m3_radius = m3_major / 2 + extra_radius;
    m3_wide_radius = m3_major / 2 + extra_radius + 0.2;

    // diameter of metric 3mm hexnut screw head
    m3_head_radius = 3 + extra_radius;
    
    
	// settings
	width = profile_width;
	length = extruder_drive_mount_plate_length;
    //this filler makes sure that the motor stays inside the chamber;
	height = plate_thickness+filler;
	hole_offsets = [
		[-width/2,  length / 2 - frame_bolts, 2.5],
		[-width/2, -length / 2 + frame_bolts, 2.5]
	];
	corner_radius = 3;

	difference(){
		// base plate
        translate([-width/2, 0, height / 2- filler]) cube([width, length, height], center = true);
		// mounting holes
		for (a = hole_offsets)
			translate(a){
				cylinder(d = frame_bolts, h = height * 2 + 2 * epsilon, center = true, $fn = 16);
				cylinder(d = frame_bolts, h = height + epsilon, $fn = 16);
			}
		// nema17 mounting holes
		translate([base_width / 2, 0, nema17_width / 2 + plate_thickness])	rotate([0, -90, 0])
			for (a = nema17_hole_offsets) translate(a) {
					cylinder(r = m3_radius, h = height * 4, center = true, $fn = 16);
					cylinder(r = m3_head_radius, h = height + epsilon, $fn = 16);
				}
	}
}

/**
* Size of the extruder drive (including the motor)
*
* @return array3 Width, length and height of the extruder drive including the motor
*/
function extruder_drive_size() = [nema17_size(nema_length)[0],extruder_drive_mount_plate_length(), 151.006];

/**
* Returns the x dimension of the extruder drive mounting plate
*
* @return float The x dimension of the extruder drive mounting plate 
*/
function extruder_drive_mount_plate_length() = (external/2-profile_width)-frame_angle_fastener_side-bed_size()[0]/3-z_axis_clamp_size()[1]/2;