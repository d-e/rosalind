/**
* Plastic blocks that connect the bed frame with the actual bed (insulation and pcb)
* and supporting modules.
*
*/

include <../general-values.scad>
use <../lib/utl.NEMA.scad>

/* Print or preview */
print();


/**
* Module that lays out the bed support bricks for easy printing.
* Prints 3 bricks, so that the 3 screws that mount the bed can also be used for calibration
*
*/
module print(){
    translate([0,0,bed_profile]) rotate([180,0,0]){
        bed_support_brick();
        translate([50,0,0]) bed_support_brick();
        translate([100,0,0]) bed_support_brick();
    }
}

/**
* The cuboids that connect the bed frame with the wooden insulation.
* They are needed because the screws that fasten the insulation must
* go through in order to enable spring suspension.
*
*/
module bed_support_brick(){
    difference(){
        cube([bed_profile,bed_profile*3,bed_profile]);
        // The vertical screws
        bed_screws();
        // The horizontal screws
        translate([-e, bed_profile/2, bed_profile/2]) rotate([0,90,0]) {
            cylinder(d=frame_bolts, h=2*bed_profile); //screw
            cylinder(d=2*frame_bolts, h=frame_bolts); //head
        }
        translate([-e, 5*bed_profile/2, bed_profile/2]) rotate([0,90,0]) {
            cylinder(d=frame_bolts, h=2*bed_profile);
            cylinder(d=2*frame_bolts, h=frame_bolts);
        }
    }
}


/**
* The vertical screws that mount the insulation plywood
* to the support bricks. 
*
*/
module bed_screws(){
    translate([bed_profile/2, 3*bed_profile/2, -e]) {
        cylinder(d=bed_screws, h=bed_profile+insulation_thickness+bed_levelling_tolerance+2e);
        translate([0, 0, bed_profile+insulation_thickness+bed_levelling_tolerance+2e-bed_screws]) cylinder(d1=bed_screws, d2=2*bed_screws, h=bed_screws );
        cylinder(d=3*bed_screws, h=2*bed_screws);
    }
}
