/**
* Angles that mount the frame together. Can be
* replaced with off the shelf metal fasteners
*
*/

use <../lib/angle-fastener.scad>
use <../vitamins/atx-psu.scad>
include <../general-values.scad>

/* Print or Preview */

print_frame_angles();
print_atx_psu_angles();


/* Modules */

/**
* Convenience module for a single angle with all it's parameters for the 
* particlular frame
*
*/
module frame_angle() {
    l = frame_angle_fastener_side;
    angle_fastener([2.5,l,profile_width],l,frame_bolts,stiffeners=1);
}

/**
* Lay out enough frame angles for one printer, for printing
* Big printers (>400mm) need 28 angles, small ones 26
* We are printing 30 because simplicity
*
*/
module print_frame_angles(){
    for (i= [0:4]){
        translate([frame_angle_fastener_side*1.1*i,0,0]) for (i= [0:5]){
            translate([0,profile_width*1.1*i,0]) rotate([90,0,0]) frame_angle();
        }
    }
}

/**
* Angles that fit an ATX psu so that it can be mounted on its bottom
*
*/
module print_atx_psu_angles(){
    for (i= [0:4]) translate([-frame_angle_fastener_side-5,profile_width*1.1*i,0]) atx_psu_angle();
}

module atx_psu_angle(){
    rotate([90,0,0]) {
        angle_fastener(size=[2.5,12,profile_width],
                        size2=[2.5,frame_angle_fastener_side,profile_width],
                        hole_position=atx_psu_holes()[1][1][2],
                        hole_diameter=frame_bolts,
                        hole_position2=frame_angle_fastener_side/2,
                        stiffeners=1,
                        multiple_holes=false);
    }
}

module atx_psu_angle_tall(){
    rotate([90,0,0]) {
        angle_fastener(size=[2.5,atx_psu_holes()[1][0][2]*2,profile_width],
                        size2=[2.5,frame_angle_fastener_side,profile_width],
                        hole_position=atx_psu_holes()[1][0][2],
                        hole_diameter=frame_bolts,
                        hole_position2=frame_angle_fastener_side/2,
                        stiffeners=1,
                        multiple_holes=false);
    }
}
