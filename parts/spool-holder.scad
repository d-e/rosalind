/**
* A cylindrical spool holder that mounts on the 
* aluminium frame
*
*/
include <../general-values.scad>

/* Constants */

d = 48; // diameter of the spool hole
h = 100; // length of the spool holder cylinder
t = 5; // Thickness of the wall of the cylinder

$fn=128;
flange_thickness = 10;
profile_width=40;
// frame_bolts=8;

/* Print or Preview */

plain();
// side_mounted();
// straight_mounted();

/* Calculated Values */

fh = profile_width-acrylic_position; //flange height

/**
* Just a plain cylinder with a mounting hole
*
* @param h float Height of the cylinder
*
*/
module plain(h=h){
    translate([0,0,h]) mirror([0,0,1]) difference(){
        cylinder(d=d, h=h);
        translate([0,0,-0.05]) cylinder(d=d-2*t, h=h+.1-40+12);
        cylinder(d=frame_bolts+1,  h=h+.1);
    }
}

/**
* A spool holder mounted to the side of the cylinder.
* Some alu profiles don't have nut slots on all 4 sides so this 
* design accomodates for that case.
*
*/
module side_mounted(){
    plain(h+fh);
    hull(){
        cylinder(d=d, h=fh);
        translate([d/2,0,fh/2]) cube([d,d,fh],true);
    }
    tt=flange_thickness+d/2; hh=10;
    difference(){
        translate([d-tt,-fh*2,0]) cube([tt,fh*4,fh]);
        subtracted();
        mirror([0,1,0]) subtracted();
        hole(); mirror([0,1,0]) hole();
    }
    module subtracted(){
        translate([d-tt,-d,-1]) cylinder(d=d, h=h);
        translate([-tt+d/2,-2*d,-1]) cube([d,d,h]);
    }
    module hole(){
        translate([d-tt,fh*1.5,profile_width/2]) rotate([0,90,0]) cylinder(d=frame_bolts, h=50);
        translate([d-tt,fh*1.5,profile_width/2]) rotate([0,90,0]) cylinder(d=frame_bolts*2, h=d/2);
    }
}

/**
* Spool holder with mounting plate
* that mounts perpendicular to the frame
*
*/
module straight_mounted(){
    difference(){
        cube([profile_width, 3*d, minimum_thickness]);
        translate([profile_width/2, profile_width/2, -e]) cylinder(d=frame_bolts, h=minimum_thickness+2e);
        translate([profile_width/2, 3*d - profile_width/2, -e]) cylinder(d=frame_bolts, h=minimum_thickness+2e);
    }
    translate([profile_width/2, 1.5*d, 0]) plain();

}