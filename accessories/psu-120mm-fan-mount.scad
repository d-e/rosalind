l = 180;
w = 130;
h = 65;
h1= 35;

difference(){
    cube([l,w,2]);
    translate([l-12,5,-1]) screw();
    translate([l-12,130-5,-1]) screw();
    translate([70,w/2,-1]) cylinder(d=120, h=10);
    translate([70-105/2,(w-105)/2,-1]) screw();
    translate([70-105/2,w-(w-105)/2,-1]) screw();
    translate([70+105/2,(w-105)/2,-1]) screw();
    translate([70+105/2,w-(w-105)/2,-1]) screw();
}

difference(){
    cube([2,w,h]);
    rotate([0,90,0]) {
        translate([-5,5,-1]) screw();
        translate([-5,w-5,-1]) screw();
        translate([5-h,w-5,-1]) screw();
        translate([5-h,5,-1]) screw();
    }
}

translate([l,0,0]) difference(){
    cube([2,130,h1]);
    rotate([0,90,0]) {
         for (i =[1:10])
        translate([0,w/11*i,-1]) hull(){ translate([-h1+10,0,0]) cylinder(d=7, h=10); translate([-10,0,0]) cylinder(d=7, h=10);}
    }
}
module screw(){
    cylinder(d=4.5, h=10);
}