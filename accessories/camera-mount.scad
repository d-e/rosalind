/**
* This is a hacky camera mount that fits rosalind #1.
* and a raspberry pi camera.
* It should fit Rosalind#2 with minor modifications.
*
*/

thickness = 4;
camera_w = 25;
camera_h = 24;
e = 0.01;
2e = 2*e;
d = 3;
$fn=32;
bed_assy_t = 17;
bed_profile = 20;
cable_w = 16;

bed_protrusion = 22;

translate([0,-camera_h-bed_assy_t,0]) difference(){
    cube([camera_w,camera_h+bed_assy_t,thickness]);
    translate([2,2,-e]) cylinder(d=3, h=2*thickness);
    translate([23,2,-e]) cylinder(d=3, h=2*thickness);
    translate([23,14.5,-e]) cylinder(d=3, h=2*thickness);
    translate([2,14.5,-e]) cylinder(d=3, h=2*thickness);
    translate([(camera_w-cable_w)/2-1,camera_h+bed_assy_t*.8,-e]) cube([cable_w+2+2e,2,thickness+2e]);
}
difference(){
    cube([25, bed_profile, bed_protrusion+thickness]);
    translate([25/2, bed_profile/2, -e]) cylinder(d=6, h=bed_protrusion+thickness+2e);
}