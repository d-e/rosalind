/**
* Nut that pushes the belt away without damaging it
* when screwing the tensioner screw 
*
*/
use <x-carriage.scad>

/* Print or preview */
tensioner(nut = true);
translate([30,0,0]) tensioner(nut = true);