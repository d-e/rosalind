/**
* Rosalind mount for e3d cyclops in bowden configuration
* This file is WIP. It is usable as in I've successfully 
* mounted cyclops on my rosalind YMMV, you might need to
* modify it a bit.
*
*/
include <../general-values.scad>
use <../lib/mendel90/vitamins/e3d_hot_end.scad>
use <../parts/x-carriage.scad>
use <../lib/nutsnbolts/cyl_head_bolt.scad>
include <../lib/mendel90/conf/config.scad>
use <../lib/CC-NC/e3d_cyclops.scad>
use <../lib/fan_duct_V3.scad>

/* Constants */

length = 50; // length of mount plate (x axis)
height = 90; // height of mount plate (z axis)

/* Print or Preview */

print();
// cyclops_bowden();
//translate([0,-x_carriage_size()[1]/2,0]) x_carriage();

/* Calculated values */

min_length = x_carriage_size()[0]/2+2*modular_x_bolt;
min_height = 2*x_carriage_size()[2]/3+2*modular_x_bolt;

if (length<min_length) echo("<font color=red>WARNING: extruder length smaller than positioning spheres distance</font>");
if (height<min_height) echo("<font color=red>WARNING: extruder height smaller than positioning spheres distance</font>");

l = max(min_length, length);
h = max(min_height, height);
w = minimum_thickness;

z_offset = 0;
cooling_z_offset = 5;
heatsink_size = [30, 18, 30];

nozzle_cooling_cyclops_size = [40, 21.47878, 72.24033];

/**
* Render the extruder mount laid out for easy printing
*
*/
module print(){
    rotate([-90,0,0]) slab();
}

/**
* Render the extruder and its mount without an extruder drive as that
* will be mounted on the frame and the filament will be fed as a
* bowden tube.
*
*/
module cyclops_bowden(){
    translate([0,0,-x_carriage_size()[2]/2]) {
        slab();
        cyclops_assembly();
        // mount_balcony();
    }
}

/**
* Print the extruder with the cooling block and fans. Includes the funnel
* assembly which has been modelled in blender because I couldn't get the
* soft shapes to work well in OpenSCAD. The .blend file is included and is
* imported here as .stl
*
*/
module cyclops_assembly(){
    // This will not work for you as it requires a model of the e3d cyclops which I do not
    // include for licensing reasons. You can find it here (https://www.thingiverse.com/thing:1018957)
    // and can put it in the CC-NC directory 
    translate([0,w+6,z_offset]) rotate([0,0,180]) print_part();;
    half_fan = 11/2; notch = 1; y_offset=w+heatsink_size[1]+nozzle_cooling_cyclops_size[1]-notch;
    translate([0, y_offset, 5]) import("lib/nozzle_cooling_cyclops.stl");
    // upright fan (extruded material cooler)
    translate([0, y_offset+half_fan, 4]) rotate([90,0,0]) fan(fan40x11);
    // slanted fan (heating block cooling fan)
    translate([0, y_offset+half_fan-nozzle_cooling_cyclops_size[1]/2, 44]) rotate([123,0,0]) fan(fan40x11);
}

/**
* The three screw holes for mounting the cyclops on the slab
*
*/
module cyclops_mount_screws(){
    translate([-30/2,0,0]){
        translate([10.5,20,0]) cylinder(d=3+mtol, h=30);
        translate([15,10,0]) cylinder(d=3+mtol, h=30);
        translate([19.5,20,0]) cylinder(d=3+mtol, h=30);
        //cap space
        h=3;
        translate([10.5,20,30-h]) cylinder(d=6, h=h);
        translate([15,10,30-h]) cylinder(d=6, h=h);
        translate([19.5,20,30-h]) cylinder(d=6, h=h);
    }
}

/**
* This is an alternative way to mount the cyclops from the holes
* it has on the top of the cooling block. It is unused here but 
* depending on the cooling solution it might be prove useful so
* it remains.
*
*/
module mount_balcony(){
    module half(){
        csize = [20,18,4];
        difference() {
            cube(csize);
            translate([0,3,0]) cylinder(d=3+mtol, h=10);
            translate([8.5,15,0]) cylinder(d=3+mtol, h=10);
        }
    }
    translate([0,w,30+z_offset]){ half(); mirror() half();}
}

/**
* The main mounting slab with the positioning spheres and the single 
* mounting bolt
*
*/
module slab(){
    x_offset = 0;
    bolt_z = x_carriage_size()[2]/2;
    difference(){
        union(){
            translate([-l/2,0,0]) cube([l,w,h]);
            translate([x_offset,0,bolt_z]) positioning_spheres(tolerance=-0.6);
        }
        // single mounting bolt
        translate([x_offset,-e,bolt_z]) rotate([-90,0,0]) cylinder(d=modular_x_bolt+mtol, h=30+2e);
        y = max(w-minimum_thickness+e,w/2+e);
        translate([x_offset,y,bolt_z]) rotate([90,0,0]) nutcatch_parallel(str("M",modular_x_bolt), l=minimum_thickness);
        translate([0,30-e,z_offset]) rotate([90,0,0]) cyclops_mount_screws();
    }
}
