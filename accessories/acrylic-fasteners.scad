/**
* This file contains all kinds of parts that have anything 
* to do with the aluminium profile or the acrylic sheets 
* enclosing the printer
*
*/

use <../parts/angle-fastener.scad>
include <../general-values.scad>
use <../vitamins/aluminium-profile.scad>


/* Basic fastener dimensions */
height = 30;
width = 12;
thickness = 3;

/* Print or preview */

// print();
preview();


/* Modules */

/**
* Render a preview of the acrylic fastener for development purposes
*
*/
module preview(){
    // acrylic clips don't work if smaller than 15mm
    if (profile_width-acrylic_position > 15) acrylic_clip();
    translate([50,50,0]) aluminium_cap(true);
    translate([0,40,0]) slot_cover(15, 14, 1.9);
    translate([-20,40,0]) slot_cover_ribbon();
}

/**
* Render the fastener laid out in a way that it is most easily printable
*
*/
module print(){
    translate([30,0,0]) rotate([0,90,0]) acrylic_angles_bottom();
    rotate([0,90,0]) acrylic_angles();
    // translate([-60,0,width]) rotate([0,90,0]) acrylic_clip();
}

/**
* Fastener for the acrylic block at the bottom of the cube. Assumes that the bottom 
* acrylic sheet will sit flush with the bottom of the cube, so this module
* ignores `acrylic_position`
*
*/
module acrylic_angles_bottom(){
    angle_fastener([3,35,20],profile_width/2,8,profile_width/2,4, stiffeners = 2, multiple_holes = false);
}

/**
* Fastener for the acrylic block at the sides of the cube. This takes into 
* consideration the `acrylic_position` variable in order to determine its size
*
*/
module acrylic_angles(){
    angle_fastener([3,profile_width-acrylic_position,20], profile_width/2, frame_bolts, profile_width/2, 4, stiffeners = 2, multiple_holes = false);
}

/**
* Shows a short part of aluminium profile for use as a boolean subtrahend
*
*/
module profile(){
   # translate([0,0,0]) rotate([0,270,0]) rotate(90) aluminium_profile();
}

/**
* Clip for fastening the acrylic sheets on the side of the printer without
* screwing them on the aluminium profile. Useful for some kinds of aluminium
* profiles that do not allow t-slot nut on all sides
*/
module acrylic_clip(){
    scale([1,1.2,1]) difference() { translate([0,acrylic_position,0]) {
            cube([width,thickness,height]);
            hull(){
                    translate([0,0,height-2]) cube([width,thickness,2]);
                    translate([0,15,-6]) rotate([30,0,0]) cube([width,thickness,4]);
            }
            difference() {
                translate([0,0,-5]) cube([width,8,11]); 
                translate([0,8,-2]) rotate([30,0,0]) cube([width,7,10]);
            }
        }
        profile();
        translate([width/2, acrylic_position+thickness*2, width]) rotate([90,0,0]) cylinder(d=2, h=10);
    }
}

/**
* Plastic part that covers the slot of the aluminium profile.
* Useful if you want to route cables through the t-slot.
*
* @param length The length of the part measured along the long axis of the t-slot
* @param width  The width of the slot to be covered. Defaults to the profile slot width
* @param thickness  The thickness of the slot 
*
*/
module slot_cover(length=10, width=custom_profile_slot_width, thickness=custom_profile_slot_thickness){
    tolerance=-0.15;
    psw = width-tolerance;
    pst = thickness;
    flange = 3;
    tooth = 0.8;
    2f = 2*flange;
    translate([0,-flange,0]) cube([length, psw+2f, pst/2]);
    difference(){
        union(){
            translate([0,0,-pst]) cube([length, psw, pst]);
            translate([0,0,-pst-tooth/2]) rotate([0,90,0]) hull(){
                cylinder(d=tooth,h=length,$fn = 32);
                translate([0,psw,0]) cylinder(d=tooth,h=length,$fn = 32);
            }
        }
        translate([0,2,-pst-tooth]) cube([length, psw-4, pst+tooth/2]);
    }
    
}

/**
* Same as slot_cover but has a slot for a ribbon cable on the outer side
*
*/
module slot_cover_ribbon(){
    slot_cover();
    difference(){
        translate([0,-3,0]) cube([10,14.60,4]);
        translate([0,-2,1]) cube([10,12.60,2]);
        translate([0,2.5,1]) cube([10,3,4]);
    }
}

/**
* Butt cap for the t-slot. Only for the large solar panel profiles
* for the time being
*
* @param down boolean   The caps of the bottom side have holes to accomodate
*                       rubber feet with alignment screws. If down is *true* 
*                       then the part is printed with holes
*
*/
module aluminium_cap(down=false){
    difference(){ 
        union() {
            cube([40,40,1], true);
            translate([0,0,3]) difference(){
                minkowski(){
                    cube([23.9,13.60,7], true); //[25.9,15.60,7]
                    cylinder(d=2, h=0.1);
                }
                if (!down) cube([23,13,8], true);
            }
        }
        if (down) translate([0,0,-10]) cylinder(d=8, h=20);
    }
}