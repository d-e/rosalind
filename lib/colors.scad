/**
* Color definitions.
*
* The initial idea was to be able to comment out various colors 
* by mass replacing `color(vitamins)` with `//` but it works
* if you want to customize your printer too.
*
*/

electronics = "green";
vitamins = "gray";
frame = "lightgrey";
components = "ghostwhite";
belts = [0.2, 0.2, 0.2];
rails = "gainsboro";
wood = "Peru";