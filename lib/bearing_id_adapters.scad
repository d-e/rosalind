/**
* Printable filler "pipes" to adapt the inner diameter of
* bearings to the diameter of bolts you happen to have 
* lying around.
*
*/


/**
* Thickness of flange for the adapters. The flange allows unflanged bearings
* to be used with timing belts
*
* @param flange_thickness float  The thickness of the printed flange 
*
*/
flange_thickness=1;

spacer(16,4,7);
//translate ([30,0,0]) spacer(16,3,7);
translate ([30,0,0]) M3_627_flanged(1);
translate ([60,0,0]) M3_627_flanged(1);

//TODO: comment these modules
module M3_688_flanged(double=false){
    translate([0,0,flange_thickness]) difference() {
        union(){
            cylinder(d=16+4, h=flange_thickness);
            cylinder(d=7.8, h=double?4:2+flange_thickness);
        }
        cylinder(d=3, h=8);
    }
}

module M3_627_flanged(tolerance=0){
    translate([0,0,flange_thickness]) difference() {
        union(){
            cylinder(d=27, h=flange_thickness);
            cylinder(d=6.8, h=7/2+flange_thickness-tolerance/2);
        }
        cylinder(d=3+tolerance, h=8);
    }
}

module M3_627(){
    translate([0,0,flange_thickness]) difference() {
        union(){
            cylinder(d=6.8, h=7/2+flange_thickness);
        }
        cylinder(d=3, h=8);
    }
}

module spacer(d,m,thickness){
    translate([0,0,flange_thickness]) difference() {
        union(){
            cylinder(d=d, h=thickness);
        }
        cylinder(d=m, h=thickness+2);
    }
}

module M4_627(){
    difference() {
        cylinder(d=6.8, h=7);
        cylinder(d=4,8, h=8);
    }
}

module M5_627(){
    difference() {
        cylinder(d=7, h=7);
        cylinder(d=5,2, h=8);
    }
}