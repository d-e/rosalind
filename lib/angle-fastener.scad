/**
* Printed angles for any required use.
*
*/

/* Preview */

size = [2.5,50,36];
tol=.5;

//angle_fastener(size,33,10.9, taper=4);
//rotate([0,90,0]) angle_fastener([2.5,60,20],30,3+tol, stiffeners=1, taper=0);
angle_fastener([2.5,30,20],30,5,stiffeners=1);

/* Modules */

/**
* Angle fastener with holes for mounting stuff at 90 degrees
*
* @param size           array3 The size of the angle
* @param hole_position  float  Position of the hole on the side of the angle. If
*                              `hole_position2` is not defined this parameter 
*                              controls the position of holes in both sides (default 25)  
* @param hole_diameter  float  The diameter of the hole on the first (or both) sides
* @param hole_position2 float  Override the hole position on side 2 (default undef)
* @param hole_diameter2 float  Override the hole diameter on side 2 (default undef)
* @param stiffeners     int    If > 0 add stiffeners on the side of the angle (default 1)
* @param taper          bool   If *true* taper the holes to accomodate countersunk screws
* @param tolerance      float  Tolerance for the hole diameters
* @param multiple_holes bool   If *true* make a series of holes on each side distanced
*                              `hole_position` or `hole_position2` apart.
*
*/
module angle_fastener(size=[2.5,30,20], hole_position=25, hole_diameter=3, size2=undef, hole_position2 = undef, hole_diameter2 = undef, stiffeners = 1, taper = 0, tolerance = 0.5, multiple_holes = true){
    hole_diameter2 = hole_diameter2==undef ? hole_diameter : hole_diameter2;
    hole_position2 = hole_position2==undef ? hole_position : hole_position2;
    size2 = size2==undef ? size : size2;
    translate([-size[0],0,0]) _face(size, hole_position, hole_diameter+tolerance, taper, multiple_holes = multiple_holes);
    
    translate([0,size2[0],size[2]]) rotate([0,180,90]) _face(size2, hole_position2, hole_diameter2+tolerance, taper,  multiple_holes = multiple_holes);
    if (stiffeners > 0) {
        linear_extrude(height = 2) polygon([[-size2[1],0],[0,0],[0,size[1]]]);
        translate([0,0,size[2]-stiffeners]) linear_extrude(height = stiffeners) polygon([[-size2[1],0],[0,0],[0,size[1]]]);
    }
}

/**
* One face of an angle fastener 
*
* @param size           array3      Dimensions of the angle
* @param hole_position  float       Position of the mounting hole
* @param hole_diameter  float       Diameter of the mounting hole
* @param taper          bool        If *true* taper the holes to accomodate countersunk screws
* @param multiple_holes bool        If *true* make a series of holes on each side distanced
*                                   `hole_position` or `hole_position2` apart     
*
*/
module _face(size, hole_position, hole_diameter, taper = 0, multiple_holes = true){
    difference(){
        cube(size);
        if(multiple_holes){
            for(i=[hole_position/2:hole_position:size[1]-hole_position/2]) {
                hole(i);
            }
        } else {
            hole(hole_position);
        }
    }
    module hole(position){
        translate([-0.01, position,size[2]/2]) rotate([0,90,0]) cylinder(d=hole_diameter+taper, d2=hole_diameter, h=size[0]+0.2);
    }
}