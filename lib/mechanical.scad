/**
* Helper modules and functions that render mechanical parts
* such as linear or rotary bearings, idlers and couplers.
*
*/
include <functions.scad>
include <colors.scad>

/* Print or preview */
bearing(name="627", hq=true, flanged=3);
translate([30,30,0]) idler();

/**
* Render a bearing from the list of bearings 
* lower in this file
*
* @param name     string The name (identification code) of the bearing
*                        as defined in the `bearings` table below.
* @param id       float  Inner diameter of the bearing
* @param od       float  Outer diameter of the bearing
* @param h        float  Height of the cylinder
* @param hq       bool   Whether to print the chamfered edges and other
*                        visual bells and whistles
* @param flanged  int    1: Flange on the bottom
*                        2: Flange on the top
*                        3: Flanges on both sides
* @param hull     bool   Whether to print the hole inside or not (used 
*                        for subtraction)
* @param tolerance float How much bigger should the bearing be rendered
*
*/
module bearing(name=undef, id, od, h, hq=false, flanged=0, hull=false, tolerance = 0){
    color(vitamins){
        if (value(name, bearings)==undef) echo(str("<font color='red'>ERROR: no such bearing ",name," </font>"));
        
        //take values from library if name is defined
        _id=name!=undef?value(name, bearings)[0]:id;
        _od=name!=undef?value(name, bearings)[1]:od;
        _h=name!=undef?value(name, bearings)[2]:h;

        //override specific dimensions if both name and dimensions are defined
        __id=(id==undef?_id:id)+10*e;//10*e is in order to make sure the rails are not auto-unioned with the 
                                     // bearings. This makes importing to blender for rendering easier
        __od=od==undef?_od:od;
        __h=h==undef?_h:h;
        
        if (hull){
            cylinder(d=__od+tolerance, h=__h);
        } else {
            if (hq&&!flanged){
                difference(){
                    hull(){
                        translate([0,0,0]) cylinder(d=__od-1,h=1);
                        translate([0,0,1]) cylinder(d=__od,h=1);
                        translate([0,0,h-1]) cylinder(d=__od,h=1);
                        translate([0,0,h]) cylinder(d=__od-1,h=1);
                    }
                    hull(){
                        translate([0,0,-1])  cylinder(d=__id+1,h=1);
                        translate([0,0,+1]) cylinder(d=__id,h=0.01);
                    }
                    hull(){
                        translate([0,0,h]) cylinder(d=__id,h=1);
                        translate([0,0,h+1]) cylinder(d=__id+1,h=1);
                    }
                    translate([0,0,-5]) cylinder(d=__id,h=__h+5);
                }
                
            } else {
                difference(){
                    union(){
                        if (flanged) {
                            if (flanged==1||flanged==3) cylinder(d=__od+2, h=1);
                            if (flanged==2||flanged==3) translate([0,0,__h-1]) cylinder(d=__od+2, h=1);
                        }
                        cylinder(d=__od,h=__h);
                    }
                    tol(Z) cylinder(d=__id,h=__h+2e);
                }
            }
        }
    }
}

/**
* Render an idler (without the teeth)
*
* @param idler_diameter          float   Diameter of the idler where the timing belt touches
* @param idler_height            float   Height of the idler including flanges
* @param idler_flange_diameter   float   Diameter of the flange of the idler
* @param idler_flange_thickness  float   Thickness of flange
* @param idler_hole_d            float   Diameter of mounting hole
*
*/
module idler(idler_diameter = 12, idler_height=8.5, idler_flange_diameter = 16, idler_flange_thickness = 1, idler_hole_d = 3){
    difference(){
        union(){
            cylinder(d=idler_flange_diameter, h=idler_flange_thickness);
            cylinder(d=idler_diameter, h=idler_height);
            translate([0,0,idler_height-idler_flange_thickness]) cylinder(d=idler_flange_diameter, h=idler_flange_thickness);
        }
        tol(Z) cylinder(h=idler_height+2e, d=idler_hole_d+mtol);
    }
}

/**
* Returns the dimensions of the given bearing
*
* @param name string The name (identification code e.g 608) of the
*                    bearing according to the `bearings` table
*
* @return array3 [id, od, h] dimensions of the bearing
*/
function bearing_size(name) = value(name, bearings);

/**
* Table of bearings with names and dimensions
* in the form `["608", [8, 22, 7] ]` 
*
*/
bearings = [
//    name   id od  h
    ["608", [8, 22, 7] ],
    ["627", [7, 19, 7] ],
    ["688", [8, 16, 5] ],
    ["623", [3, 10, 4] ],
    ["LM10UU", [10, 20, 22.5] ],
    ["LM10LUU", [10, 20, 55] ],
    ["LM8UU", [8, 14, 24] ],
    ["LM8LUU", [8, 14, 48] ],
];

/**
* Cylindrical coupler of shafts of different diameters
*
* @param from float   Initial diameter
* @param to   float   Target diameter
*
*/
module coupler(from=5,to=8){
    difference(){
        color(vitamins) cylinder(h=25, d=18);
        translate([0, 0, -e]) cylinder(h=25+2e, d=to+s+2e);
    }
}
