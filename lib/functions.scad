X = 0; Y = 1; Z = 2;

data=[ ["abc",1],["bca",2],["cad",3],["d",4],["a",5],["b",6],["c",7],["d",8],["e",9] ];


//find values from a hashtable by their keys
function value(key, data) = data[search([key],data)[0]][1];

echo (value("877", data));

function typeof(a) = a==undef   ?"undefined":
                  len(a)==0     ?"empty":
                  a[0]==undef   ?"number":
                  a[0]+0==undef ?"string":
                  "vector";


include <nutsnbolts/data-access.scad>;                
include <nutsnbolts/data-metric_cyl_head_bolts.scad>; 
function nut_height(key) = data_screw_fam[search([key],data_screw_fam)[0]][9];

/**
* Return the closest larger number divisible by 5
* Convenience function used to thicken things so 
* that screws don't protrude out of them as
* screws come in lengths of increments of 5
*
* @param number float  Just a number 
*
* @return int The nearest larger number divisible by 5 
*/
function ceil5(number) = ceil(number/5)*5;

//thanks trygon (http://forum.openscad.org/Determining-what-data-type-a-variable-is-holding-td16111.html)

module rotate_about(v,a) {
    translate(v) rotate(a) translate(-v) child(0);
}

module tol(axis){
    if (axis == X) translate([-e,0,0]) children();
    if (axis == Y) translate([0,-e,0]) children();
    if (axis == Z) translate([0,0,-e]) children();
}

include <../general-values.scad>
include <colors.scad>

/**
* Print vitamins with a different color and only if `show_vitamins`
* is true
*
* @param render bool Whether to display anything or not
*/
module vitamin(show = show_vitamins){
    if (show) color(vitamins) children();
}

/**
* Print rails with a different color and only if `show_rails`
* is true
*
* @param render bool Whether to display anything or not
*/
module rail(show = show_rails){
    if (show) color(rails) children();
}

/**
* Print wooden parts with a different color and only if `show_wood`
* is true
*
* @param render bool Whether to display anything or not
*/
module wood(show = show_wood){
    if (show) color(wood) children();
}


/**
* Show 3D models of electronics with a different color and
* only if `show_electronics` is true
*
* @param render bool Whether to display anything or not
*/
module electronics(show = show_electronics){
    if (show) color(electronics) children();
}

/**
* 3D annotation text with 
*
* @param height the height of the extruded text (default 2)
*
*/

module annotext(text="annotation", height=2, font="ocrb", halign="left"){
    linear_extrude(height) text(text, font=font, halign=halign);
}