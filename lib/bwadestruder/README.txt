                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:1831961
Greg's B'Wadestruder - geared B'struder - bowden extruder for MK7 / MK8 and similar gears - v2 by VanessaE is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

This Thing is a combination of tempo502's B'struder (http://www.thingiverse.com/thing:711401), with the idler that takes PC4-M6 fittings, and ronanwarrior's high-resolution Greg's Wade Reloaded extruder gears (http://www.thingiverse.com/thing:97377).  Gear ratio is unchanged from ronanwarrior's, at 47:9 (5.222:1).  760 steps/mm if you're using a standard MK8 hob gear and 1/16 microstepping.  With the pictured motor, this extruder can push filament at 30 mm/sec reliably (too much faster and my particular motor/controller/driver combo stalls).

This extruder is designed for 1.75 mm filament, but with a suitable idler arm, it should be possible to use 3 mm filament.

The B'struder "plate" has been heavily modified to remove all of the former mounting holes, the thickness has been adjusted slightly, space has been added for a bearing where the motor axle hole used to be, one edge has been extended out and thickened to hold the motor off to the side, large holes have been added to reduce its mass a bit, a small hole was added to the spring's end stop so that a screw can be driven in to tension the spring, and a few other holes and nut traps have been added to the upright part to allow the top plate to attach to it.

On the large gear, the top and bottom surfaces have been re-faced and the whole thing has been tidied up from the triangulated mess that STL requires (just to make it easier to edit), and I modified it to accept an M5 bolt (rather than the usual M8).  I also thinned the center part down to accommodate.

On the small gear, I increased the clearance around the nut trap and added a flat to the side, to leave room for a larger range of set screw lengths.

The gears mesh together quite well, and have minimal backlash.

You can either print and use the idler arm included here, or grab one of tempo502's other variants.

The support objects for the base plate and top plate are provided here as separate STLs, in case you want to give them weak/sparse print settings, or if you want to let your slicer use its automatic supports.  Note that the support objects for the top plate are upside down in the STL.

**UPDATE: 2016-10-22:** Somehow the motor's screw holes got lost from the revised 2020 bracket.  Fixed.

UPDATE: 2016-10-21.6: Replaced the bracket with a much shorter version that should keep everything out of way of anything attached to the 2020 extrusion.
UPDATE: 2016-10-21.5: Fixed the bracket.  I forgot to add a hole for the small gear to pass through.  Derp. :-)
UPDATE: 2016-10-21.4: Replaced the mounting bracket with an "L" version that should be more useful.
UPDATE: 2016-10-21.3: Added a mounting bracket to fit 2020 extrusion.  Holes are sized for M4 screws.
UPDATE: 2016-10-21.2: Noticed and fixed an error in the base plate (a vertex got moved for some reason, screwing up the top of the baseplate where the motor goes).
UPDATE: 2016-10-21.1: New .blend with all the screws, bearings, and other hardware included, a few existing bits simplified (well, sorta), and redundant stuff removed.  The actual printable parts are unchanged.  Added an exploded view diagram, should help with assembly.
UPDATE: 2016-10-20:  Replaced all files with version 2.  The first version had a problem with the M5 nylock nut working loose enough to allow the bolt and large gear to lean sideways, making the extruder noisy and somewhat inaccurate during retracts.  Now the end of the bolt is held steady by an upper plate screwed onto the base plate, into which one of the two 625ZZ bearings has been moved.  The base plate has been modified to add the necessary holes and nut traps for those screws.  This new version is MUCH quieter since the gears now stay well-aligned, and it can reliably turn about 10 percent faster, as well.  The printed gears themselves are unchanged from v1.

**Hardware needed:**

2 - 625ZZ bearings.
3 - 623ZZ bearings.
1 - M5 hex bolt, 35 to 40 mm length.  You may be able to get by with 30 mm, depending on how tightly your particular parts fit together and whether you use the optional washer under the big gear.
1 - M5 nylock nut, or two regular M5 nuts if you can get thin ones.
1 - MK7 or MK8 hobbed gear, or similar.
1 - PC4-M6 pneumatic/bowden fitting, or equivalent.
1 - Allen/socket-head M4 screw, 5 to 10 mm length.
1 - self-drilling screw of some kind, 3 to 3.5 mm in diameter, 15 to 20 mm length.
1 - suitable idler arm spring, 20 to 25 mm length.
\* - a few washers to fit the M5 bolt.
\* - assorted M3 screws, nuts, and washers to put it all together.  Lengths range from 8 to 45 mm.

**Assembly:**

1. Print everything and remove the support bits :-)
2. Press the 625zz bearings into the recesses in the base plate and top plate
3. Press two M3 nuts into the two recesses on the inside faces of the upright part of the base plate.  You may have to trim the holes and reach for your soldering iron.
4. Trim the bottom of the small gear as needed (first-layer flare/elephant's foot) and insert an M3 nut into the trap.
5. Thread an 8 to 9 mm M3 screw into the side hole and part-way into that nut.
6. Place the small gear onto the motor shaft, round end first, and thread that screw in *just* enough to align the gear with the flat on the shaft.
7. Insert four 10 to 12 mm M3 screws into the base plate and use them to mount the motor.  Take care to orient the motor's connector/wires correctly for your setup.  If you intend to use the 2020 mounting bracket, insert it between the motor and the base plate, and use 15-17 mm screws instead.
8. Assemble the idler arm:
 8a. Use your hobby knife and a 2 mm drill bit to clear out the filament path.
 8b. Trim the two holes at the end of the arm and press-in two 623ZZ bearings.
 8c. Insert a third 623ZZ bearing into the recess in the middle.
 8d. Insert an M3 nut into the trap next to it.
 8e. From the nut-side of the arm (that is, the side that faces away from the base plate), thread a 12 to 14 mm M3 screw into that nut and tighten it down.  Don't over-tighten.
 8f. Carefully line up the threads and install the PC4-M6 fitting into the end.  After installing it, rotate it a little so that one of the flats lines up parallel with the side of the arm (so that it doesn't scrape against the base plate).
9. File or grind a flat spot into the M5 bolt, ranging 15 to 25 mm from the underside of the head.  It need not be particularly deep, maybe 1 mm, but make the surface as flat as possible, or it can be slightly concave if that's easier.  Exactly where you put this flat depends on the specific hobbed gear you'll be using and how you orient it.
10. Align the large gear with the small gear and the hole in the base plate's bearing, flat side toward the bearing, and press the M5 bolt in and all the way through.  Be sure you seat the head fully into the recess in the large gear.
 10a. Optional:  before pressing the M5 bolt in, add a washer between the large gear and the bearing.
11. Insert an M3 nut into the trap on the underside of the base plate.
12. Using a 20 to 22 mm M3 screw and a washer under the head, install the idler arm into the base plate, with the curved side toward the hobbed gear (duh :-) ).  You may find it necessary to insert one or more washers between the arm and the base plate, to align the arm precisely with the hobbed gear when it goes in later.
13. Insert a short M4 Allen head screw into the end of the idler tension spring and snap them into place between the idler arm and the base plate's end stop piece, with the screw toward the end stop.  They may be loose, depending on the spring length and screw head thickness.
 13a. Pick the spring and screw up off the desk and snap them in again.
 13b. Get down on the floor, retrieve the spring, get another M4 screw, and snap it in again.
 13c. Contact your local astrophysics expert and inquire how the spring ended up in another dimension, get a new spring, and snap them in again.
 13d. Drink a shot of your favorite liquor, start praying to your favorite deity, get another spring and screw, and snap them in again.
14. Press the idler arm against the spring, and hold it out of the way while you slip one or two washers over the M5 bolt.
15. Add your hobbed gear.  If your gear has a grooved hob (rather than a simple flat profile), put that end in first.  The number of washers you added in the previous step depend on how well you can align your gear's hob groove, if it has one, with the filament path provided by the idler arm.  Thread the hobbed gear's grub screw in *just* enough to grab the flat on the M5 bolt, but not enough to prevent it moving up and down freely.
16. Thread an M5 nylock nut down onto the bolt, and tighten it down kinda tight (but not so much that you risk damaging anything) to draw all the parts together one last time, and then  loosen it *just barely* enough that the printed gears can turn easily.  The tighter you leave the nut against the hobbed gear, the slower your maximum retraction speed will be, owing to the additional load it places on the motor due to friction between the base plate, the hobbed gear, the washers between them, and whatever parts of the lower bearing that rub on the base plate.  On the other hand, a tighter nut also means more perfect alignment between the bolt, printed gears, and bearings. You can also use two regular M5 nuts here, one jammed hard against the other, so long as there is room for them, and the printed gears are still free to turn.
17. Tighten down the grub screw on the hobbed gear.
18. Place the top plate over the stack, taking care to align its bearing with that bolt.
19. Thread in two 9 mm M3 screws into the two holes in the top plate that have countersink flats, and tighten them into the two nuts you pressed into the base plate's upright part.
20. Push a 45 to 50 mm M3 screw and washer into the diagonally-oriented hole running through the bottom-most parts of the top plate and base plate. Secure the other end with a washer and nut.
21. Drive in a 15 to 20 mm self-drilling sheet metal screw into the hole opposite the tension spring.  Something 3 to 3.5 mm diameter, with sharp threads that can bite the plastic nicely. Make sure the end of the screw engages the Allen screw in the end of the spring.
22. Rotate the printed gears so that the small gear's set screw is exposed to the access hole on the side of the base plate.
23. Adjust the height of the small gear, aligning it precisely with the large gear (I used the centerline of the herringbone as my guide), and tighten down the small gear's screw.  Don't over-tighten.
24. Apply a tiny amount of oil or grease to the washer(s) you inserted between the hobbed gear and the base plate, and a bit more to the printed gears' teeth.  Don't get any oil/grease on the hobbed gear or it'll get onto your filament.

The whole assembly can then be mounted to your machine by taking two of the screws out of the back of the motor and replacing them with longer ones, or by using the 2020 mounting bracket and a couple of 24 mm (or longer) M4 screws and suitable T-nuts.  These T-nuts seem to be popular, in particular:  http://www.thingiverse.com/thing:1573410  

The bracket should be useful for mounting onto other things besides 2020 extrusion.

The 2020 extrusion found in the .blend comes from http://www.thingiverse.com/thing:280318 (100mm version).

The "All Parts" STL isn't meant to be printed, it's just here so that you can use the "Thingview" function to take a closer look at the assembly.

This extruder was inspired by ask0045's geared B'struder (http://www.thingiverse.com/thing:1694683) and follows a somewhat similar design.


# Print Settings

Printer Brand: RepRap
Printer: Prusa i3
Rafts: No
Supports: No
Resolution: 0.1 to 0.2 mm
Infill: 50% to 70%

Notes: 
Print everything at a somewhat high infill density - 50% ought to do for the gears, base plate, and top plate.  You'll need to flip the top plate, its support objects (if you're using them), and the two gears upside down for best results.  The support objects should all snap off easily with a small flat-blade screwdriver and/or by hand.  Pay close attention to your slicer's results -  it's not hard to cause the slicer to over-fill the small support objects, and make sure your bed is exceptionally clean - good first-layer adhesion is especially important for this Thing.  Print the idler arm with around 70% infill; print it slowly, with lots of fan, and set your hotend temperature somewhat lower than you ordinarily might (but without losing inter-layer adhesion).  Use 0.1mm layer height for the gears and idler arm, and 0.2 mm for the base plate and top plate.

# How I Designed This

I started by loading the B'struder plate, and removing all of the holes, save for the central one and the small one that holds the idler arm.  I then un-triangulated the whole thing to make it easier to work on, created pockets for the two bearings, and so on.

I then added an extension for the motor, with the two aligned to place a 0.15mm gap between the two sets of gear teeth.

Finally, I created the top plate starting from an ordinary cube, with lots of edge subdivision and beveling to create the "L" shape and rounded corners.

All work done in Blender.

The .blend file contains a number of other elements not meant to be printed, e.g. stand-ins for the M5 bolt and nut, hobbed gear, motor, etc. to make visualizing the whole thing easier.  Please note that I got the gear from coelestinian's STL-ified QU-BD extruder model (http://www.thingiverse.com/thing:39299).  I believe the motor is Installer's model (http://www.thingiverse.com/thing:871140).

The two bearings depicted in the .blend are from my 7½-shot-pellet-based 625ZZ model (http://www.thingiverse.com/thing:1831737).