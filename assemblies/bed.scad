/**
* The heated bed assembly
* Usually you don't have to print anything here. 
* If your bed pcb is significantly smaller than the
* bed frame you might want to print support beams
* Check out `print_support_beams()`
*
*/

include <../lib/Fractional_T-Slot_20x_mm_OpenSCAD_Library/T-Slot.scad>
include <../general-values.scad>
include <../config/calculated-values.scad>
use <../parts/bed-clamps.scad>
use <../parts/bed-support-bricks.scad>
use <../parts/leadscrew-nut-clamp.scad>
use <../parts/z-axis-clamp.scad>
use <../lib/utl.NEMA.scad>
use <../lib/nutsnbolts/cyl_head_bolt.scad>
use <../lib/angle-fastener.scad>
use <../vitamins/aluminium-profile.scad>
use <../lib/mechanical.scad>

/* Constants */

screw3_angle_center = 10; //the distance from the center of the hole to the edge of the aluminum extrusion for the angle that holds the bed on the right.
bed_countersunk_screws = true;

/* Print or Preview */

// print_support_beams();
bed();

/* Calculated Values */

//This is the max Y dimension when the constraining factor are the Z leadscrews
autoy = external-2*bed_profile-nema17_size(nema_length)[0]-leadscrew_nut_d;

lx = _bed_platform_length;
ly = bed_platform_width=="auto"?autoy:bed_platform_width;

half_nema = nema17_size(nema_length)[0]/2;
disty = bed_dist_y;

if (bed_platform_width<autoy) echo("Warning: build platform too small");

/**
* Lay out the support beam for printing. Check out `support_beam()` for
* support beam documentation.
*
*/
module print_support_beams(){
    support_beam();
    translate([-150,150,0]) rotate(90) mirror([0,1,0]) support_beam();
}

/**
* The assembly of the heated bed.
*
*/
module bed(){
    l = bed_insulation_length;
    w = bed_insulation_width;
    // these are also echoed in th
    echo(str("Bed wood size: ", l,"x",w));

    /**
    * This is the basic assembly of the bed. Includes the bed frame the 
    * clamps and the leadscrew pillow block.
    *
    */
    translate([0,0,-bed_profile/2]){
        xprofile(true); 
        if (bed_platform_supports==2) mirror([0,1,0]) xprofile();
        yprofile(); mirror() yprofile();
    }
    
    // show the pcb in light gray
    color("LightGrey") translate([0,0,heated_bed_thickness/2+insulation_thickness+bed_levelling_tolerance]) heated_bed_pcb();

    /**
    * Screws that fasten the heated bed to the assembly below. 
    * (or rather, their holes)
    *
    */
    module bed_screw(){
        translate([heated_bed_length/2-bed_screws*1.5,heated_bed_width/2-bed_screws*1.5, -bed_assembly_thickness]){
                cylinder(d=bed_screws, h=bed_assembly_thickness*2); //let's have more than enough height
        } 
    }

    /**
    * This is the representation of the heated bed pcb
    *
    */
    module heated_bed_pcb(){
        difference(){
            cube([heated_bed_length,heated_bed_width,heated_bed_thickness], center=true);
            // 4 screws
            bed_screw(); mirror() bed_screw();
            mirror([0,1,0]) { bed_screw(); mirror() bed_screw(); }
            // In case you ever want to mount the bed directly to the frame without insulation
            // use only 3 screws otherwise you won't be able to level
            // mirror([0,1,0]) {translate([-bed_platform_length/2 - 3*bed_screws,0,0]) bed_screw();}
        }
    }
    
    // if the pcb is way smaller than the platform (e.g. in case you plan to upgrade your printer but only have a 200x200 pcb) then 
    // print a diagonal support beam that passes through the point the hole in the pcb is
    if ((bed_platform_length>250 || bed_platform_width>250) && bed_platform_width-heated_bed_width > 50 && bed_platform_length-heated_bed_length > 50) {
        support_beam();
        mirror([0,1,0]) support_beam();
    }
    //TODO: the beam overlaps one side of the bed assembly
    
    /* Calculated values */
    insulation_translation = [0, -(disty-heated_bed_width)/4, insulation_thickness/2 + bed_levelling_tolerance];

    // positions of the three bed support bricks
    support_brick_1_offset = [lx/2, (ly-disty-bed_clamps_size_back()[1]-3*bed_profile)/2, -bed_profile];
    //the y of the above statement is the average y dimension of the following statements (simplified)
    support_brick_2_offset = [-lx/2-bed_profile, -disty/2, -bed_profile];
    support_brick_3_offset = [-lx/2-bed_profile, ly-disty/2-bed_clamps_size_back()[1]-3*bed_profile, -bed_profile];

    // the bed insulation (preferably plywood in order to have some structural stiffness)
    color("peru") difference(){
        translate(insulation_translation) cube([bed_insulation_length, bed_insulation_width, insulation_thickness], center=true);
        translate(support_brick_1_offset) bed_screws();
        translate(support_brick_2_offset) bed_screws();
        translate(support_brick_3_offset) bed_screws();
    } 

    // can we get the glass over the bed mounting screws?
    // most beds need a little drilling for that. If you are
    // not up to it just set `bed_countersunk_screws` to false
    glass_width = bed_countersunk_screws ?
        heated_bed_width :
        heated_bed_width-6*bed_screws; //2 bed screws (one on each side) and another for 
                                       //tolerance and all this x2 so that we can fit the heads.

    glass_translation = [0, 0, insulation_thickness+glass_thickness/2+heated_bed_pcb_thickness+bed_levelling_tolerance];
    % translate(glass_translation) cube([heated_bed_length, glass_width, glass_thickness], center=true);

    // show the support bricks
    translate(support_brick_1_offset) bed_support_brick();
    translate(support_brick_2_offset) bed_support_brick();
    translate(support_brick_3_offset) bed_support_brick();
}

/**
* One x-side of the bed frame
*
* @param endstop_adjuster bool Whether this side of the bed (has an endstop)
*                              (Only has meaning if bed is supported on both sides)
*
*/
module xprofile(endstop_adjuster=false) { 
    module bed_angle_fasterer(){
        translate([(lx-2*bed_profile)/2-s,bed_profile/2+s,-bed_profile/2])
            angle_fastener(size=[2.5,1.5*bed_profile,bed_profile],hole_position=1.5*bed_profile);
    }
    translate([0,-disty/2+bed_profile/2,0]){
        translate([s,0,0]) rotate([0,90,0]) color("silver") bed_profile(lx-2*bed_profile-3*s); 
        bed_angle_fasterer(); mirror() bed_angle_fasterer();
    }
    xclamps(); mirror() xclamps(endstop_adjuster);
}

/**
* One y-side of the bed frame
*
*/
module yprofile() {
    color("silver") translate([(lx-bed_profile)/2, ly/2-disty/2-bed_clamps_size_back()[1], 0]) rotate([0,90,90]) bed_profile(ly); 
}

/**
* Pillow blocks for the linear bearings that slide up and down the Z rails, as 
* well as the leadscrew nut
*
* @param endstop_adjuster bool Whether the rendered pillow block has a placeholder
*                         for the endstop adjuster screw 
*
*/
module xclamps(endstop_adjuster=false) {
    inner_clamp = bed_clamps_size()[1]-bed_clamps_size_outer()[1];
    translate(bed_x_clamps_position()) mirror([0,1,0]) bed_clamp(endstop_adjuster,bearing=true);
    internal = external - profile_width*2;
    translate([0,-internal/2+nema17_size(nema_length)[0]/2,0]) mirror([0,1,0]) leadscrew_nut_clamp(with_nut=true);
}

/**
* Pillow blocks for the linear bearings of the y side. Unused in the current 
* configuration. 
*
*/
module yclamps() {
    inner_clamp = bed_clamps_size()[1]-bed_clamps_size_outer()[1];
    translate([lx/2+inner_clamp+bed_profile,ly/3,-bed_clamps_size()[2]/2]) rotate([0,0,-90]) bed_clamp(bearing=true);
}

/**
* Diagonal beam that supports the bed if the pcb dimensions are significanly smaller
* than the bed frame (useful if you plan to upgrade heated bed size)
*
*/
module support_beam() {
    
    dist = 4; //distance of holes from the edge of the bed.
    // coordinates of bed corner
    bed_corner = [(bed_platform_length)/2 - heated_bed_length - screw3_angle_center, heated_bed_width/2];
    // coordinates of bed hole
    support_hole = bed_corner + [dist, -dist];
    
    o = bed_profile/6;
    2o = 2*o;
    
    // the x and y coords of the four corners of the support beam
    beam_polygon = [
        [beam_line_x(disty/2-bed_profile, o)   , disty/2-bed_profile                   ],
        [beam_line_x(disty/2-bed_profile, -2o) , disty/2-bed_profile                   ],
        [-bed_platform_length/2                , beam_line(-bed_platform_length/2,-2o) ],
        [-bed_platform_length/2                , beam_line(-bed_platform_length/2, o)  ],
    ];

    // draw the beam
    difference(){
        // draw a polygon and extrude it
        translate([0,0,-bed_profile]) linear_extrude(bed_profile) 
            polygon(beam_polygon);
        // subtract the holes for the screws
        translate([-bed_profile/2,-0,-bed_profile/2]) translate(beam_polygon[1]) rotate([90,0,0]){
            screw_hole();
        }
        translate([0,bed_profile/2,-bed_profile/2]) translate(beam_polygon[2]) rotate([0,90,0]){
            screw_hole();
        }
        // nut traps for the nuts of the bed mounting screws
        // rotated 45 degrees because the support beam is diagonal
        translate([support_hole[0],support_hole[1], -bed_profile/2]) rotate(-45) {
            nutcatch_sidecut(str("M",bed_screws));
            cylinder(d=bed_screws, h=bed_profile);
        }
    }
    /**
    * The space for the bed support screws on the support beam
    *
    */
    module screw_hole() {
        cylinder(d=bed_screws+1, h=bed_profile);
        translate([0, 0, bed_profile/2-bed_screws]) cylinder(d=bed_screws+4, h=bed_profile);
    }
    /**
    * Linear equation angled 45° 
    *
    * @param x  float  The x coordinate of which we need to find the y (variable)
    * @param x1 float  The x coord of a point through which the line passes (coefficient)
    * @param y1 float  The y coord of a point through which the line passes (coefficient) 
    * @param offset float Distance from the line that goes through [0,0], [1,1] etc.
    *
    */
    function beam_line_general(x, x1, y1, offset) = (x - x1+offset)+y1+offset;

    /**
    * Linear equation parallel to the one that starts from an inside edge of the bed 
    * to its perpendicular edge at an angle of 45° and passes from the support hole of the bed.
    *
    * @param x       The variable (The x point we are searching the y value for) 
    * @param offset  Distance from the line that goes through the mounting hole
    *
    */
    function beam_line(x, offset=0) = beam_line_general(x, support_hole[0], support_hole[1], offset);

    /**
    * The linear equation of a line angled 45° parallel to the one that
    * passes through the bed mounting hole solved as per x.
    * The equation is symmetrical because 45° means slope = 1 so this 
    * equation is equivalent to 
    * 
    *     function beam_line_general_x(y, x1, y1, offset) = (y - y1-offset)+x1-offset;
    *
    *. 
    *
    * @param y       float The y whose x we are looking for
    * @param offset  float The distance from the [0,0] [1,1] line  
    *
    */
    function beam_line_x(y, offset=0) = beam_line_general(y, support_hole[1], support_hole[0], -offset);
}

/**
* The size of the heated bed
*
* @return array3  The x and y dimensions of the heated bed. Z dimension returned is 0 
*/
function bed_size() = [lx,disty,0];

/**
* The position of the z-rail bearing pillow blocks mounted
* on the x-side of the bed assembly 
*
* @return array3 The coordinates of the center of the clamps
*                (center of the hole) 
*/
function bed_x_clamps_position() = [lx/2-bed_clamps_size()[0]-s,
                                    -disty/2-bed_clamps_size()[1]+bed_clamps_size_outer()[1]-s,
                                    -bed_clamps_size()[2]/2];