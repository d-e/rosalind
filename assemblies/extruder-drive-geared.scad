/**
* Just an adapter file that calls the stl for the geared extruder.
* The rosalnid has not been tested with this kind of extruder.
*
*/
include <../general-values.scad>
include <../parts/extruder-mount.scad>

module extruder_drive_geared(){
    import("../lib/bwadestruder/geared-bstruder-wades_v2.stl");
}

extruder_drive_geared();