/**
* Representation of the bottom of the printer along with all electronics
* Also used to print a 2D plan to help print the holes on the base plywood
*
*/


/*                                                         *\
  ==========================================================

          ATTENTION: Export this as SVG, not STL

  ==========================================================
\*                                                          */

include <../general-values.scad>
include <../lib/functions.scad>
include <../lib/utl.NEMA.scad>
use <../parts/z-axis-clamp.scad>
use <../parts/angle-fastener.scad>
use <../vitamins/atx-psu.scad>

/* Constants */
spacer = profile_width/2;
$fn = 64;
usb_plug_space = 70;
power_cable_space = 70;
// The x coordinate is so that the PSU gives enough space for the angle fasteners to not collide with the profile.
psu_position = [profile_width*1.5, max(profile_width*1.5, nema17_size()[1] + frame_angle_fastener_side + power_cable_space, z_axis_clamp_size()[0]) + frame_angle_fastener_side, base_plate_thickness];
arduino_position = [psu_position[0]+psu_size[0]+spacer+4.5, profile_width+usb_plug_space+nema17_size()[0]+2];
// This is the position of the motor base (not the start of the shaft as usual)
z_motor_position = [external/2,nema17_size()[0]/2+profile_width,base_plate_thickness+2e];

/* Print or Preview */

// base(solo = true);
holes_plan();

/* Calculated Values */
psu_screw_diff = [12, -frame_angle_fastener_side, -e-base_plate_thickness*2];
_psu_screw_distance = psu_type=="ATX"?psu_size - psu_screw_diff:psu_screw_distance;

/**
* Show the base without electronics and vitamins so that 
* it can be exported to dxf and 2D printed
* The resulting paper can be used as a guide to drill the
* holes on a piece of plywood
*
*/
module holes_plan(){
    projection() {
        difference(){
            base(solo = true);
            translate([profile_width,profile_width,0]) cube([150,1,10]);
        }
        frame_crosses();
        translate(psu_position) psu_positioner() cross();
        translate(arduino_position+[-5,-2.5,0]) arduino_positioner() cross();
        translate(z_motor_position) nema_hole_positioner(bodyL=nema_length) cross();
    }
}

/**
* The wooden base of the printer with holes wherever needed
*
*/
module base(solo = false){
    difference(){
        wood() cube([external, external, base_plate_thickness]);
        frame_bolts();
        translate(psu_position) psu_positioner() base_bolt(d=psu_bolts);
        translate(arduino_position+[-5,-2.5,0]) arduino_positioner() base_bolt(d=electronics_bolts);
        translate(z_motor_position- [0,0,-base_plate_thickness-e]) nema_holes(bodyL=base_plate_thickness+2e);
    }
    if (!solo){
        translate(arduino_position+[0, 0, base_plate_thickness]) arduino();
        translate(psu_position) {
            psu();
            atx_psu_mount();
        }
        translate(z_motor_position + [0,0,nema_length]) nema17(nema_length);
    }
}

/**
* A cylinder representing one sheet metal screw that will 
* mount the frame to the base of the printer
*
*/
module frame_bolt(){
    cylinder(d=frame_bolts*1.2, h=base_plate_thickness+2e);
}

/**
* Positioning module for frame bolts and crosses. These
* represent the sheet metal screws that will mount the
* frame to the base of the printer positioned at the four
* corners of the base 
*
*/
module frame_bolts_crosses_positioner(){
    translate([0, 0, -e]) {
        translate([profile_width/2, profile_width/2, 0]) children();
        translate([external-profile_width/2, external-profile_width/2, 0]) children();
        translate([external-profile_width/2, profile_width/2, 0]) children();
        translate([profile_width/2, external-profile_width/2, 0]) children();
    }
}


/**
* Cylinders representing the sheet metal screws that will 
* mount the frame to the base of the printer positioned
* at the four corners of the base
*
*/
module frame_bolts(){
    frame_bolts_crosses_positioner() frame_bolt();
}

/**
* 3D cross of planes that shows as a cross when projected to a plane
*
*
*/
module cross(r = 10, h = 10, t=0.5){
    cube([r, t, h], center = true);
    cube([t, r, h], center = true);
}

/**
* Cylinders representing the sheet metal screws that will 
* mount the frame to the base of the printer positioned
* at the four corners of the base
*
*/
module frame_crosses(){
    translate([0, 0, -e]) {
        translate([profile_width/2, profile_width/2, 0]) cross(r = frame_bolts);
        translate([external-profile_width/2, external-profile_width/2, 0]) cross(r = frame_bolts);
        translate([external-profile_width/2, profile_width/2, 0]) cross(r = frame_bolts);
        translate([profile_width/2, external-profile_width/2, 0]) cross(r = frame_bolts);
    }
}

/**
* Cylinders representing the 4 bolts that mount the psu to the 
* wooden floor of the printer
*
*/
module psu_positioner(){
    // if the psu is a computer power supply then it doesn't 
    // have screw holes on the bottom so we will mount it using
    // printed angles. Thus the screws will be outside the boundary
    // of the PSU
    translate(psu_screw_diff/2) {
        children();
        // The 6 below counteracts the psu_screw_diff above
        translate([atx_psu_holes()[1][1][0]-6, 0, 0]) children();
        translate([0, _psu_screw_distance[1], 0]) children();
        translate([_psu_screw_distance[0], _psu_screw_distance[1], 0]) children();
    }
}

/**
* Angle fasteners to mount an atx psu to the 
* wooden floor of the printer
*
* @param  
*
* @return  
*/
module atx_psu_mount(){
    a = -profile_width/2+6;
    translate([a,0,0]) rotate(90) atx_psu_angle_tall();
    translate([atx_psu_holes()[1][1][0]-profile_width/2, 0, 0]) rotate(90) atx_psu_angle();
    translate([a+profile_width, _psu_screw_distance[1]-frame_angle_fastener_side, 0]) rotate([0,-90,0]) rotate(-90) frame_angle();
    translate([a+profile_width+_psu_screw_distance[0], _psu_screw_distance[1]-frame_angle_fastener_side, 0]) rotate([0,-90,0]) rotate(-90) frame_angle();
}

/**
* Arduino mounting holes
*
*/
module arduino_positioner(){
    translate([7.3,18.2,-e]) children();
    translate([56,16.7,-e]) children();
    translate([7.3,93.6,-e]) children();
    translate([56,101,-e]) children();
}

/**
* One part bolt sized and positioned so that it can penetrate the
* base plywood
*
*/
module base_bolt(d = 3){
    cylinder(h=base_plate_thickness+2e, d=d); 
}


/**
* Show the arduino mega and ramps 
* Arduino stl: https://www.thingiverse.com/thing:34174/files (CC-BY-SA)
*
*/
module arduino(){
    translate([13.1,55,10]) rotate(90) import("../lib/RAMPS1_4_decimated.stl");
}

/**
* Show a cube representing the power supply unit
* (WIP)
* Arduino stl: https://www.thingiverse.com/thing:34174/files (CC-BY-SA)
*
*/
module psu(){
    vitamin() {
        if (psu_type == "ATX"){
            atx_psu();
        } else {
            cube(psu_size);
        }
    }
}