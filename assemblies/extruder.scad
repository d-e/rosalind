/**
* Functions returning sizes regarding the extruder assembly
*
*/

/**
* The size of the extruder assembly itself (non-printed parts)
*
* @return array3 The size of the non-printed parts of the extruder assembly 
*/
function extruder_size() = [40,30,50];

/**
* The size of the mounting plate of the extruder
*
* @return array3 The size of the mounting plate of the extruder 
*/
function extruder_mount_size() = [40,50,50];

/**
* The size of the fan that cools the filament that just came out of 
* the nozzle 
*
* @return The size of the fan that cools the filament that just came
* out of the nozzle.
*/
function object_cooling_fan_size() = [40,10,40];